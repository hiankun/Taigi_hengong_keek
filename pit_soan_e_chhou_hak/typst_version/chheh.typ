#let chheh(
    title,
    content,
) = {
    show heading: it => [
        #set align(center)
        #v(0.4em)
        #it
        #v(0.2em)
    ]
    set page(
        paper: "a4",
        margin: (top: 4.5em, bottom: 1.0em, left:3.5em, right: 3.5em),
        header:[
        #h(1fr)
        #title
        #h(1fr)
        #counter(page).display()
        ],
    )
    set text(
        font: "Linux Libertine",
        size: 20pt,
    )
    set par(
        leading: 0.5em,
        first-line-indent: 1em,
        justify: true,
    )
    set align(left)
    set enum(
        tight: true,
        spacing: 0.8em,
    )
    content
}

#let example(foo) = {text(size: 15pt, [#foo])}

#let example_equation(//FIX
    c1, c2,
    text1,
    text2,
) = {
    grid(
        columns: (c1,c2),
        rows: (auto),
        example[#text1],
        example[#text2],
    )
}

#let bl(
    addr, link_text,
) = {
    link(addr)[#underline(text(blue, [#link_text]))]
}

//FIX: workaround
#let sp=[#h(1em)] //to indent the fist paragraph
#let cc=[#h(0pt,weak:true)] //to concate the breaking line

//tiámkhui kìhō
#let tk=[
#h(0em,weak:true)#move(dy:0.8em,rotate(180deg)[,])#h(0em,weak:true)
]
//tō͘tiám
#let tt=[
#h(0em,weak:true)$dot.c$#h(0em,weak:true)
]
