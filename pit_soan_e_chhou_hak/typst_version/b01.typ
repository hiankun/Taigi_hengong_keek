#import "chheh.typ": chheh, example, example_equation
#let title = [PIT SOÀN Ê CHHO͘  HA̍K.]
#let subtitle = [Thâu chi̍t pún.]
#let press_info = [
Ē-MN̂G:\
KÓ͘-LŌNG-SŪ, CHUI-KENG-TÔNG ÌN.\
1897.]

//chheh phôe
#grid(
    columns: (1fr),
    rows: (1fr,6fr,1fr),
    align(center + horizon,
        text(size:36pt, [#title]),
    ),
    align(center + horizon,
        text(size:30pt, [#subtitle]),
    ),
    align(center + horizon,
        text(size:18pt, [#press_info]),
    ),
)

#show: content => chheh(
    [#title],
    content
)
#outline(
    title: [LŌE IÔNG]
)

#include "./b01_chapters/soat_beng.typ"
#include "./b01_chapters/oe_thau.typ"
#include "./b01_chapters/sou_bok.typ"
#include "./b01_chapters/ki_siau_e_hoat_tou.typ"
#include "./b01_chapters/ka_hoat.typ"
#include "./b01_chapters/kiam_hoat.typ"
#pagebreak()
#include "./b01_chapters/seng_hoat.typ"
#include "./b01_chapters/pun_hoat.typ"
#pagebreak()
#include "./b01_chapters/sio_sou.typ"
#include "./b01_chapters/sio_sou_e_ka_hoat_kiam_hoat.typ"
#include "./b01_chapters/sio_sou_seng_hoat.typ"
#include "./b01_chapters/sio_sou_pun_hoat.typ"
#include "./b01_chapters/sun_hoan_e_sio_sou.typ"
#include "./b01_chapters/hoa_hoat.typ"
#include "./b01_chapters/tsu_teng_ka_hoat.typ"
#pagebreak()
#include "./b01_chapters/tsu_teng_kiam_hoat.typ"
#include "./b01_chapters/tsu_teng_seng_hoat.typ"
#include "./b01_chapters/tsu_teng_pun_hoat.typ"
