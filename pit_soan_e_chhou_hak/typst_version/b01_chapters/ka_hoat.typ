#import "../chheh.typ": chheh, example, example_equation, sp, cc

= KA-HOAT.

#sp Ēng kúi-nā tiâu-siàu ha̍p-kiōng tsòe chi̍t-tiâu, hō
tsòe *Ka-hoat*, iā thang kiò tsòe *Ke-hoat*.

#example[
$3$ ke $5$, ha̍p-kiōng poeh-ê: $8$ ke $7$, ha̍p-kiōng $15$. Kiám-chhái
ū saⁿ-tiâu á-sī sì-tiâu, chiū nn̄g-tiâu tio̍h siong-ka, khòaⁿ ha̍p-#cc
kiōng kúi-ê, chiah só͘ tit-tio̍h ê siàu tio̍h kap tē-saⁿ-tiâu siong-#cc
ka. Siat-sú $5, 7, 8, 4$ beh siong-ka, chiū tio̍h kóng: $5$ ke $7$, ha̍p-#cc
kiōng $12$; $12$ ke $8$, ha̍p-kiōng $20$; $20$ ke $4$, ha̍p-kiōng $24$.
]

//p.09
Kiám-chhái siàu ê ūi m̄-nā chi̍t-ê, chiū iàu-kín
tio̍h jīn-bêng i-ê-ūi, chiàu chhù-sū lâi pâi-lia̍t-i.
Thâu-tiâu tio̍h thán-hoâiⁿ siá; tē-jī tiâu ē-bīn tio̍h
hoâiⁿ-lia̍t, kì toaⁿ-ê-ūi tú-tú tī toaⁿ-ê-ūi-ē, tsa̍p-ê-ūi
tī tsa̍p-ê-ūi-ē, pah-ê-ūi tī pah-ê-ūi-ē, í-siōng kok tùi
i-ê-ūi pâi-lia̍t. Ta̍k tiâu tio̍h chhin-chhiūⁿ án-ni hoâiⁿ-#cc
lia̍t; toaⁿ tùi toaⁿ, tsa̍p tùi tsa̍p, pah tùi pah, chheng,
bān, lóng chiàu i-ê *lūi*, hō͘ sûi-ūi tsòe it-ti̍t lo̍h-lâi.
Pâi-lia̍t bêng-pe̍k tio̍h u̍ih-*hûn* keh-kài, seng tùi
toaⁿ-ê-ūi lâi ké-khí, khòaⁿ kai-ū jōa-tsōe; *lân-san-ê*
tio̍h kì tī *pún-ūi*-ē, chiūⁿ-tsa̍p chiū tio̍h chìn chi̍t-#cc
ūi, it-tsa̍p chìn $1$, jī-tsa̍p chìn $2$. Kok-ūi tio̍h chiàu
chit-ê hoat-tō͘ lâi-sǹg, chiū ōe tit-tio̍h tsóng-siàu.

#example_equation(
    7fr,1fr,
[
#sp Siat-sú ū saⁿ tiâu siàu, chi̍t tiâu sī $8974$, chi̍t tiâu sī $867$,
koh chi̍t tiâu sī $4795$: beh ha̍p-kiōng tsòe chi̍t tiâu; hoat-tō͘
tio̍h chhin-chhiūⁿ *piⁿ-thâu* ê khoán-sit. Thâu-chi̍t-tiâu tio̍h siá
tī téng-bīn tē-it lia̍t; tē-jī tiâu tio̍h siá tē-jī lia̍t, toaⁿ
tī toaⁿ ê ūi tsa̍p tī tsa̍p ê ūi, pah tī pah ê ūi; tē-saⁿ
tiâu tio̍h siá tē-saⁿ lia̍t, chiàu thâu pâi. Pâi-lia̍t bêng-#cc
pe̍k, tio̍h u̍ih chi̍t hûn keh kài, tùi toaⁿ ê ūi seng sǹg-#cc
khí, chiū $5$ $7$ $4$ siong-ka kai tit $16$; taⁿ $6$ sī lân-san-ê
tio̍h kì $6$ *lok* pún ūi ê hûn ē, it-tsa̍p tio̍h chìn chêng-#cc
ūi chòe $1$. (Ha̍k-seng teh sǹg ê sî, tio̍h ho͘ kóng, $5$,
$12$, $16$, kì $6$, chìn $1$.) Tsa̍p ê ūi só͘ chìn ê $1$ kap $9$ $6$ $7$ siong-ka
kai tit $23$, lân-san ê $3$ tio̍h kì lok pún ūi ê hûn ē, jī-tsa̍p tio̍h
koh chìn chêng-ūi tsòe $2$. (Tio̍h ho͘ kóng, $1$, $10$, $16$, $23$, kì $3$,
chìn $2$.) Pah ê ūi só͘ chìn ê $2$ kap $7$ $8$ $9$ siong-ka kai tit $26$,
lân-san ê $6$ tio̍h kì lok pún ūi ê hûn ē, jī tsa̍p koh chìn chêng-#cc
ūi tsòe $2$. (Tio̍h ho͘ kóng, $2$, $9$, $17$, $26$, kì $6$, chìn $2$.) Chheng
ê ūi só͘ chìn ê $2$ kap $4$ $8$ siong-ka kai tit $14$, lân-san
ê $4$ tio̍h kì lok pún ūi ê hûn ē, it tsa̍p koh chìn chêng-#cc
ūi tsòe $1$. (Tio̍h ho͘ kóng, $2$, $6$, $14$, kì $4$, chìn $1$.) Bān
ūi bô siàu thang siong-ka, chiū só͘ chìn ê $1$ tio̍h kì tī
bān ūi ê hûn-ē. Taⁿ hûn-ē só͘ kì ê $14636$ chiū-sī *chóng-#cc
siàu*.
],

[$ 
8974&\
867&\
4795&\
#overline(offset:-1em,[14636])&
$],
)

#example_equation(
    7fr,1fr,
[
#sp Siat-sú ū gō͘ tiâu siàu, chiū-sī $9280$, $68094$, $298$,
$48079$, $7155$, beh siong-ka, chiū tio̍h chhin-chhiūⁿ piⁿ-#cc
thâu ê khoán-sit pâi-lia̍t, u̍ih hûn keh kài, lâi ho͘
//p.10
kóng, $5$, $14$, $22$, $26$, kì $6$, chìn $2$. $2$, $7$, $14$, $23$, $32$, $40$, kì $0$,
chìn $4$. $4$, $5$, $7$, $9$, kì $9$. $7$, $15$, $23$, $32$, kì $2$, chìn $3$. $3$, $7$, $13$,
kì $3$ chìn $1$ kì chêng-ūi. Taⁿ hûn-ē só͘ kì ê $132906$ chiū-sī tsóng-#cc
siàu.
],

[$ 
9280&\
68094&\
298&\
48079&\
7155&\
#overline(offset:-1em,[132906])&
$
],
)

#pagebreak()
#sp Ē-bīn ê siàu ha̍k-seng tio̍h siong-ka.

#example[
+ #grid(columns:(1fr,1fr,1fr,1fr,1fr),
    rows:(auto),
    [$
24&\
underline(32)&
    $],
    [$
68&\
underline(75)&
    $],
    [$
101&\
underline(660)&
    $],
    [$
406853&\
underline(259870)&
    $],
    [$
504&\
underline(540)&
    $],
)
+ #grid(columns:(1fr,1fr,1fr,1fr),
    rows:(auto),
    [$
100&\
101&\
underline(333)&
    $],
    [$
#underline(offset:2.5em,[5687])&\
384&\
275&
    $],
    [$
2224&\
3331&\
underline(1324)&
    $],
    [$
87654&\
39265&\
underline(77889)&
    $],
)

+ Che kúi-nā tiâu-siàu tio̍h pâi-lia̍t lâi siong-ka. $105$, $4$, $26$; koh, $150$, $1007$, $84$, $3205$.
+ Chiaⁿ-ge̍h ū jī-tsa̍p-káu ji̍t, jī-ge̍h ū saⁿ-tsa̍p ji̍t: Nn̄g-ge̍h lóng kiōng kúi-ji̍t?
+ Chi̍t-chiah tsûn ū nn̄g-pah-saⁿ-tsa̍p-sì lâng, koh chi̍t-chiah ū saⁿ-pah-gō͘-tsa̍p-jī: Nn̄g chiah lóng-kiōng kúi lâng?
+ Chi̍t-lâng chhoe-it thàn pah-jī chîⁿ, chhoe-jī thàn nn̄g-pah-sì-tsa̍p-gō͘, chhoe-saⁿ thàn chi̍t-pah-saⁿ-tsa̍p-sì; taⁿ mn̄g, Saⁿ-ji̍t lóng-kiōng thàn jōa-tsōe-chîⁿ?
+ Bó͘-mi̍h Chím khì bóe chi̍t-siang-ôe ta̍t-tit sì-pah chîⁿ, chi̍t-niáⁿ-kûn ta̍t chi̍t-chheng liân saⁿ-tsa̍p chîⁿ, chi̍t-niáⁿ-saⁿ ta̍t gō͘-chheng saⁿ-pah gō͘-tsa̍p chîⁿ, chi̍t-ki thâu-thok ta̍t chi̍t-chheng la̍k-pah saⁿ-tsa̍p-jī chîⁿ; Só͘ bóe lóng-kiōng jōa-tsōe chîⁿ?
+ Só͘ pâi-lia̍t ê siàu tio̍h siong-ka. #grid(columns:(1fr,1fr,1fr),
    rows:(auto),
    [$
8866442&\
7676853&\
2987586&\
3434343&\
underline(10368109)&
    $],
    [$
7654321&\
1020304&\
5600700&\
2386427&\
underline(5560470)&
    $],
    [$
#underline(offset:5.0em,[386275])&\
123456&\
380642&\
102354&\
18384&
    $],
)
+ Thiⁿ-ē pun gō͘-ê *tōa pō͘-chiua* kap tsōe-tsōe hái-sū, taⁿ siat-sú *A-se-a * ū $696725000$ lâng, *A-hui-lī-ka* ū $7067000$, *Pak A-be̍k-lī-ka* ū $49280480$ lâng, *Lâm A-be̍k-lī-ka* ū $21356870$ lâng, *Au-lô-pa* ū $282959505$ lâng, *Hái-sū* $25898788$ lâng; chiū thiⁿ-ē lóng-kiōng jōa-tsōe lâng?
+ Sî-cheng chi̍t-tiám phah chi̍t-ē, nn̄g-tiám phah nn̄g-ê, lóng-kiōng sǹg, tùi chi̍t-tiám kàu poeh tiám tio̍h phah kúi-ē? Koh; tùi chi̍t-tiám kàu tsa̍p-jī tiám tio̍h phah kúi-ē?
//p.11
+ *Ē-mn̂g* tah *hé-hun-tsûn* beh khì *Tāi-Eng kok*, tsúi-lō͘ tio̍h sī Ē-mn̂g kàu *Hiong-káng* $930$ lí, Hiong-káng kàu *Si̍t-la̍t* $4790$ lí, Si̍t-la̍t kàu *Pin-nn̂g-sū* $1270$ lí, Pin-nn̂g-sū kàu *Sek-lân* $3037$ lí, Sek-lân kàu *Âng-hái* $7113$ lí, kè Âng-hái $4360$, kè *Ai-ki̍p* ê tōa-káng $270$ lí, tōa-káng kàu *Bí-lī-tāi* $3180$, Bí-lī-tāi kàu *Se-pan-ngá* ê lâm-hng $3270$ lí. Se-pan-ngá ê lâm-hng kàu Tāi-Eng kok $4080$ lí. Taⁿ mn̄g: Ē-mn̂g kàu Si̍t-la̍t kúi lí? Ē-mn̂g kàu Pin-nn̂g-sū kúi lí? koh, Ē-mn̂g kàu Tāi-Eng kok kúi lí?
+ Siat-sú *Ti̍t-lē* ū saⁿ-chheng bān lâng, *Kang-so͘* sì-chheng bān lâng, *An-hui* saⁿ-chheng la̍k-pah bān, *Soaⁿ-sai* chi̍t-chheng gō͘-pah bān, *Soaⁿ-tang* saⁿ-chheng chi̍t pah bān, *Hô-lâm* nn̄g-chheng sì-pah bān, *Siám-sai* chhi̍t-chheng gō͘-pah bān, *Kam-siok* chi̍t-chheng la̍k-pah bān, *Chiat-kang* nn̄g-chheng poeh-pah bān, *Kang-sai* saⁿ-chheng nn̄g-pah bān, *Sù-chhoan* nn̄g-chheng nn̄g-pah bān, *Hok-kiàn* chi̍t-chheng poeh-pah bān, *Kńg-tang* nn̄g-chheng saⁿ-pah bān, *Kńg-sai* poeh-pah bān, *Hûn-lâm* gō͘-pah gō͘-tsa̍p bān, *Kùi-chiu* gō͘-pah gō͘-tsa̍p bān; Chiū tsa̍p-poeh séng lóng-kiōng jōa-tsōe lâng?
]

