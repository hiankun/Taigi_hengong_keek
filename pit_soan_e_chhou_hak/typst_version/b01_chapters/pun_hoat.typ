#import "../chheh.typ": chheh, example, example_equation, tk, sp, cc

#v(2em) //FIX
= PUN-HOAT.

#sp Pun-hoat iā thang kiò-tsòe Tû-hoat.

Pun-hoat kap Kiám-hoat siāng-lūi, hoat-tō͘ kap
sêng-hoat saⁿ-tùi-hoán. Kiám-chhái chi̍t-tiâu-siàu
ài pun tsòe kúi-nā tiâu, kiám-hoat hùi-khì, pun-hoat
khah kín-khoài.

//p.21
#example[
#grid(columns:(1.5fr,5fr,1fr),
    rows:(auto),
    [$
    18&&\
    6&dots.h.c &1\
#line(length:1.5em)&\
    12&&\
    6&dots.h.c &1\
#line(length:1.5em)&\
    6&&\
    6&dots.h.c &1\
#line(length:1.5em)&#line(length:1.5em)\
&&3
    $],
    [
#sp Siat-sú beh pun tsa̍p-poeh kho͘
hō͘ la̍k lâng. Chiàu kiám-hoat sûi-#cc
lâng tio̍h tāi-seng tit-tio̍h chi̍t-#cc
kho͘, lóng-kiōng la̍k-ê. Hit-ê la̍k-ê tio̍h khàu-#cc
khí-lâi, chhun tsa̍p-jī, iā koh chhin-chhiūⁿ án-#cc
ni khàu lóng bêng-pe̍k chiàu tò-pêng só͘ pâi-lia̍t
ê hoat-tō͘. Nā-sī ha̍k-seng í-keng ha̍k-si̍p kiú-#cc
kiú-ha̍p-sò͘ chiū tsai $6 times 3=18$, só͘-í ēng la̍k lâi
pun $18$ ōe pun-tit tú-tú saⁿ-pái, chiàu iū-pêng
só͘ kì-tsài.
    ],
    [$
    6)&underline(18)\
    &3
    $],
)
]

#sp Pun-hoat, tio̍h ēng *goân-siàu* kiò tsòe si̍t, *tû-siàu*
kiò-tsòe hoat. Tāi-seng tio̍h siá si̍t siàu, tò-pêng
*phoat chi̍t-ê phoat*, *phoat-gōa* siá hoat-siàu.

Siat-sú hoat-siàu ū toaⁿ-ūi tiāⁿ-tiāⁿ, tio̍h u̍ih-hûn
tī si̍t-siàu-ē, seng khòaⁿ si̍t-siàu tò-pêng thâu-chi̍t-#cc
ūi ōe chiok hoat-siàu kúi-pē lâi kì tī hûn-ē tsòe só͘
tit-tio̍h ê siàu. Tē-jī ūi í-hā lóng tio̍h án-ni.

#example_equation(
    6.5fr, 1.5fr,
[
#sp Siat-sú ū goân-siàu $69303$, beh ēng $3$ tsòe hoat lâi pun-i, chiū
tio̍h pâi-lia̍t chhin-chhiūⁿ piⁿ-thâu ê khoán-sit.
Seng khòaⁿ $6$, (chiū-sī $6$ bān,) chiok $3$ nn̄g-pē,
chiū tio̍h siá $2$, (chiū-sī $2$ bān) tī hûn-ē. Koh
khòaⁿ $9$, (chiū-sī $9$ chheng,) chiok $3$ saⁿ-pē tio̍h
siá $3$ (chiū-sī $3$ chheng,) tī hûn-ē. Koh khòaⁿ $0$, (chiū-sī bô-pah,)
bōe pun-tit chiū tio̍h siá $0$ tī hûn-ē thang lâu pah ê ūi. Koh
khòaⁿ $3$, (chiū-sī $3$ tsa̍p) chiok $3$ chi̍t-pē, tio̍h siá $1$, (chiū-sī it-#cc
tsa̍p) tī hûn-ē. Koh khòaⁿ $0$ bōe pun-tit tio̍h siá $0$ tī hûn-ē.
#v(0.5em,weak:true)
Ha̍k-seng teh-sǹg ê sî tio̍h ho͘ kóng, $3$ pun $6$, tit $2$, kì $2$. $3$
pun $9$, tit $3$, kì $3$. $3$ pun $0$, bô, kì $0$. $3$ pun $3$, tit $1$, kì $1$. $3$ pun
$0$, bô, kì $0$.
],
[$
3)underline(69030)&\
23010&
$],
)

#example_equation(
    6.5fr, 1.5fr,
[
#sp Piⁿ-thâu só͘ pâi-lia̍t ê siàu ha̍k-seng tio̍h ho͘
lâi sǹg: $4$ pun $8$, tit $2$, kì $2$. $4$ pun $0$, bô, kì $0$.
$4$ pun $0$, bô, kì $0$. $4$ pun $4$ tit $1$, kì $1$. $4$ pun $8$
tit $2$, kì $2$. $4$ pun $0$, bô, kì $0$.
],
[$
4)underline(800480)&\
200120&
$],
)

#sp Kiám-chhái si̍t tò-pêng thâu-chi̍t-ūi ê siàu pí hoat
khah sòe, bô thang pun, chiū tio̍h nn̄g-ūi *saⁿ-liân* lâi-#cc
pun, só͘ tit-tio̍h ê siàu kì tī hûn-ē. Pun-liáu kiám-#cc
//p.22
chhái ū chhun, chiū hit-ê chhun tio̍h kap tē saⁿ ūi
saⁿ-liân lâi koh pun; í-hā lóng tio̍h án-ni.

#example_equation(
    6.0fr, 1.5fr,
[
#sp Siat-sú ū si̍t-siàu $2376$, beh ēng $4$ tsòe hoat lâi pun-#cc
i. Tio̍h pâi-lia̍t chhin-chhiūⁿ piⁿ-thâu ê khoán-sit.
],
[$
4)underline(2376)&\
594&
$],
)
#v(0.5em,weak:true)
#example[
#sp Seng khòaⁿ si̍t-siàu tò-pêng ê $2$, (chiū-sī $2$ chheng) pí hoat-#cc
siàu ê $4$ khah-sòe, bô thang pun, chiū tio̍h khòaⁿ nn̄g-ūi ê $23$
(chiū-sī $23$ pah) saⁿ-liân lâi pun-i, tit-tio̍h $5$ chhun $3$. Kì $5$ tī
hûn-ē; só͘ chhun ê $3$ (chiū-sī $3$ pah $= 30$ tsa̍p) tio̍h kap tē saⁿ ūi
ê $7$ *saⁿ-chham* tsòe $37$ (chiū-sī $37$ tsa̍p) lâi pun-i, tit-tio̍h $9$
chhun $1$. Só͘ tit-tio̍h ê $9$ tio̍h kì tī hûn-ē; só͘ chhun ê $1$ (chiū-#cc
sī it-tsa̍p) kap tē-sì-ūi ê $6$ saⁿ-liân tsòe $16$; pun-i chiū só͘ tit-tio̍h
ê $4$ tio̍h kì tī hûn-ē.
#v(0.5em,weak:true)
Ha̍k-seng tio̍h ho͘ kóng $4$ pun $2$, bô thang pun; $4$ pun $23$ tit
$5$, chhun $3$, kì $5$; $4$ pun $37$ tit $9$ chhun $1$ kì $9$; $4$ pun $16$ tit $4$, kì $4$.
]
#v(0.5em,weak:true)
#example_equation(
    6fr, 1.5fr,
[
#sp Siat-sú $185205$ tsòe si̍t, beh ēng $3$ tsòe hoat lâi pun-i.
#v(0.5em,weak:true)
Pâi-lia̍t bêng-pe̍k, khòaⁿ si̍t tò-pêng ê $1$ (chiū-#cc
sī $1$ tsa̍p-bān) pí $3$ khah-sòe, só͘-í tio̍h nn̄g-ūi saⁿ-#cc
liân $18$; (chiū-sī $18$ bān;) taⁿ $3$ pun $18$ tit $6$, (chiū-#cc
sī $6$ bān) kì $6$ tī hûn-ē. Koh $3$ pun $5$ tit $1$, (chiū-#cc
sī $1$ chheng) chhun $2$, kì $1$. Só͘ chhun ê $2$ (chiū-sī $2$ chheng $=$
$20$ pah), kap tē-sì ūi ê $2$, saⁿ-chham tsòe $22$; (chiū-sī $22$ pah;)
taⁿ $3$ pun $22$ tit $7$ (chiū-sī $7$ pah) chhun $1$, kì $7$. Só͘ chhun ê $1$
(chiū-sī $1$ pah $= 10$ ê tsa̍p) kap tsa̍p-ūi ê $0$ saⁿ-chham tsòe $10$;
taⁿ $3$ lâi pun $10$ tit $3$ (chiū-sī $3$ tsa̍p) chhun $1$, kì $3$. Só͘ chhun
ê $1$ (chiū-sī $1$ tsa̍p) kap toaⁿ-ūi ê $5$ saⁿ-chham tsòe $15$; taⁿ $3$ pun
$15$ tit $5$, kì $5$ tī hûn-ē.
],
[$
3)underline(185205)&\
61735&
$],
)
#v(0.5em,weak:true)
#example[
#sp Ha̍k-seng tio̍h ho͘ kóng $3$ pun $1$, bô thang pun; $3$ pun $18$ tit
$6$ kì $6$; $3$ pun $5$ tit $1$ chhun $2$, kì $1$; $3$ pun $22$ tit $7$ chhun $1$,
kì $7$; $3$ pun $10$ tit $3$ chhun $1$, kì $3$; $3$ pun $15$ tit $5$, kì $5$.
]
#v(0.5em,weak:true)
#example_equation(
    6fr, 2fr,
[
#sp Koh piⁿ-thâu só͘ pâi-lia̍t ê siàu tio̍h ho͘ lâi
sǹg, kóng $5$ pun $3$, bô thang pun; $5$ pun $36$,
tit $7$ chhun $1$, kì $7$; $5$ pun $10$ tit $2$, kì $2$; $5$ pun
$0$, bô, kì $0$; $5$ pun $4$, bô thang pun, kì $0$; $5$ pun
$42$ tit $8$ chhun $2$, kì $8$; $5$ pun $25$, tit $5$, kì $5$.
],
[$
5)underline(3600425)&\
720085&
$],
)

#sp Beh sǹg chit-hō ê siàu, iàu-kín tio̍h ha̍k-si̍p Kiú-#cc
kiú-ha̍p-sò͘, kàu ōe liām lóng-bô-chhò.
#v(0.5em,weak:true)
Kiám-chhái ài tsai só͘ pun ê, tio̍h á-m̄-tio̍h, chiū
só͘ tit-tio̍h ê siàu, thang ēng hoat-siàu sêng-i; só͘
//p.23
tit-tio̍h, kap si̍t-siàu saⁿ-tâng, chiū tsai bô m̄-tio̍h.

#example_equation(
    6fr, 2fr,
[
#sp Siat-sú ēng $5$ tsòe hoat lâi-pun $43215$ ê si̍t-#cc
siàu. Só͘ tit-tio̍h ê siàu chiū-sī $8643$. Ēng hoat-#cc
siàu, $5$, lâi sêng-i chiū koh tit-tio̍h $43215$. Khó-#cc
kiàn, pun, bô m̄-tio̍h.
],
[$
5)underline(43215)&\
8643&\
5&\
overline(43215)&
$],
)

#sp Chiah ê siàu ha̍k-seng tio̍h sǹg, iā tio̍h *chhut-pîn-#cc
kù* pun bô m̄-tio̍h.

#example[
+ $6482 div 2$; $39066 div 3$; $48840008 div 4$.
+ $1234 div 2$; $690468 div 3$; $241512 div 4$.
+ $4338270 div 5$; $6487404 div 6$; $393491 div 7$.
+ $690200 div 8$; $123456789 div 9$; $987654321 div 9$.
+ Saⁿ-lâng thàn chi̍t-kho͘-gûn, khì-ōaⁿ chiū tit-tio̍h $1140$ chîⁿ thang pun. Ta̍k lâng tit-tio̍h jōa-tsōe? #h(1fr)$380$ chîⁿ.
+ Sì-khek chi̍t-tiám-chheng, chiū $52$ khek kúi tiám-cheng? #h(1fr)Tek $13$.
+ Gō͘-lâng beh pun saⁿ-kho͘, chi̍t-kho͘ ōaⁿ-tit $1135$ chîⁿ, chiū ta̍k-lâng tit-tio̍h jōa-tsōe? #h(1fr)$618$ chîⁿ.
+ La̍k lâng saⁿ-kap tsòe-kang thàn $3030$ chîⁿ; ta̍k-lâng tio̍h pun jōa-tsōe? #h(1fr)$505$ chîⁿ.
+ Chhit-lâng saⁿ-kap *kò͘*-tsûn beh khì bó͘ só͘-chāi; tsûn-*sè* $3290$ chîⁿ, ta̍k-lâng tio̍h chhut jōa-tsōe? #h(1fr)$470$ chîⁿ.
+ Poeh-ge̍h-ji̍t-kú ê chhù-sè $4560$ chîⁿ, chiū chi̍t-ge̍h-ji̍t jōa-tsōe? #h(1fr)$570$ chîⁿ.
+ Lâng beh khì bó͘ só͘-chāi $783$ lí hn̄g, kiâⁿ $9$ ji̍t chiah ōe kàu: ta̍k-ji̍t tio̍h kiâⁿ kúi-lí? #h(1fr)Tek $87$.
+ Ēng $8280$ chîⁿ bóe poeh-*pau* chheh, múi-pau káu-pún, chiū chi̍t-pún jōa-tsōe chîⁿ? #h(1fr)Tek $115$.
+ Chi̍t-keng chhù ū $8$ keng pâng, ta̍k-keng-pâng ū saⁿ-*hù*-thang, ta̍k-hù-thang ū nn̄g-*sìⁿ*-mn̂g ta̍k-sìⁿ ū $5$ *phìⁿ* po-lê pîⁿ-pîⁿ tōa; sai-hū *ba̍uh* ji̍p-po-lê lóng-kiōng tio̍h $20400$ chîⁿ: taⁿ mn̄g, chi̍t-phìⁿ po-lê jōa-tsōe chîⁿ? #h(1fr)Tek $85$.
]

#sp Siat-sú hoat siàu ū kúi-nā ūi, si̍t kap hoat í-keng
pâi-lia̍t hó, m̄-sái u̍ih-hûn tī ē-tóe, tio̍h phoat chi̍t
phoat tī iū-pêng. Khòaⁿ si̍t ê tsó-pêng tio̍h tàu kúi-#cc
ūi, chiah ū kàu thang hō͘ hoat pun, chí kúi-ūi tio̍h
hun-piat tsòe thâu-chi̍t-ê sin ê si̍t, siūⁿ ōe chiok hoat
//p.24
ê siàu kúi-pē, kì tī iū-pêng phoat-gōa tsòe *tek-siàu*,
kap hoat lâi saⁿ-sêng, siá tī thâu-chi̍t-ê sin ê si̍t ê
ē-tóee, u̍ih-hûn keh-kài lâi kiám, só͘ chhun-ê siá tī
hûn-ē. Tùi goân ê si̍t só͘ sīn ê ūi koh chhú-chhut
chi̍t-ūi, tàu-lo̍h-lâi chham tsòe tē-jī sin ê si̍t, khòaⁿ
ōe chiok hoat kúi-pē, koh kì tsòe tek-siàu tē-jī-űi
iû-goân kap hoat saⁿ-sêng, siá tī tē-jī sin ê si̍t ê ē-#cc
tóe, u̍ih-hûn lâi kiám, í-hā lóng tio̍h án-ni. Kiám-#cc
chhái ū-sî chhú-chhut chi̍t-ūi *tàu-chham* tsòe sin ê
si̍t, bô kàu thang pun, chiū tio̍h kì $0$ tsòe tek-siàu,
tsài-koh chhú-chhut chi̍t-ūi tàu-chham lâi pun, kiám-#cc
chhái iū bô kàu-gia̍h, tio̍h koh kì $0$; ta̍k-pái chhú-chhut
ta̍k-pái tio̍h kì tek-siàu. Goân-si̍t í-keng pun kàu
chīn, chiū iū-pêng phoat-gōa sī tek-siàu, lō͘-bé ê só͘-#cc
sêng kiám-chhun-ê, chiū-sī *lân-san*, bōe pun-tit-ê.

#example_equation(
    5.0fr, 3.0fr,
[
#sp Siat-sú ū si̍t ê siàu $5889498$, beh ēng $254$ tsòe hoat lâi pun.
Si̍t kap hoat í-keng pâi-lia̍t hó, tio̍h phoat chi̍t phoat tī si̍t ê
iū-pêng; si̍t ê tsó-pêng tio̍h tàu saⁿ-ê
ūi, chiū-sī $588$, chiah ū kàu thang hō͘
hoat pun. Chí saⁿ ê ūi tio̍h hun-piat
tsòe thâu-chi̍t-ê sin ê si̍t, ōe chiok hoat
ê siàu nn̄g pē, tio̍h kì $2$ tī iū-pêng phoat-#cc
gōa tsòe tek-siàu. Ēng $2$ lâi sêng hoat
ê $254$, chiū tit-tio̍h $508$, tio̍h siá tī $588$
ê ē-tóe, u̍ih-hûn keh-kài lâi kiám, só͘
chhun ê $80$, siá tī hûn-ē; tùi goân ê si̍t
só͘ sīn ê ūi, chhú-chhut chi̍t ūi $9$, tàu
lo̍h-lâi chham tsòe $809$, chiū-sī tē-jī sin
ê si̍t, ōe chiok hoat saⁿ pē, tio̍h kì $3$,
tsòe tek-siàu ê tē-jī ūi.],
[$
254&)5889498(23187\
&#h(0.5em)underline(580)\
&#h(1.0em)809\
&#h(1.0em)underline(762)\
&#h(1.5em)474\
&#h(1.5em)underline(254)\
&#h(2.0em)2209\
&#h(2.0em)underline(2032)\
&#h(2.5em)1778\
&#h(2.5em)underline(1778)\
$],
)
#v(0.5em,weak:true)
#example[Ēng $3$ sêng $254$,
tit-tio̍h $762$, tio̍h siá tī $809$ ê ē-tóe, u̍ih-hûn lâi kiám, só͘ chhun
ê $47$ siá tī hûn-ē; tùi goân ê si̍t só͘ sīn ê ūi, koh chhú-chhut $4$,
tàu lo̍h-lâi chham tsòe $474$, chiū-sī tē saⁿ sin ê si̍t, ōe chiok hoat
chi̍t pē, tio̍h kì $1$, tsòe tek-siàu ê tē-saⁿ ūi. Ēng $1$ sêng $254$,
tit-tio̍h $254$, tio̍h siá tī $474$ ê ē-tóe, u̍ih-hûn lâi kiám, só͘ chhun
ê $220$ siá tī hûn-ē; tùi goân ê si̍t só͘ sīn ê ūi, koh chhú-chhut $9$,
tàu lo̍h-lâi chham tsòe $2209$, chiū-sī tē-sì sin ê si̍t, ōe chiok hoat
//p.25
poeh pē, tio̍h kì $8$ tsòe tek-siàu ê tē-sì ūi. Ēng $8$ sêng $254$, tit-#cc
tio̍h $2032$ tio̍h siá tī $2209$ ē-tóe, u̍ih-hûn lâi kiám, só͘ chhun ê
$177$ siá tī hûn-ē; koh chhú-chhut goân ê si̍t só͘ sīn ê $8$, tàu lo̍h-#cc
lâi chham tsòe $1778$, chiū-sī tē-gō͘ sin ê si̍t, ōe chiok hoat chhit
pē, kì $7$ tsòe tek-siàu ê tē-gō͘ ūi. Ēng $7$ sêng $254$, tit-tio̍h $1778$,
siá tī $1778$ ê ē-tóee, u̍ih-hûn lâi kiám, tú-tú pun tùi-tú, lóng bô
chhun. Taⁿ goân ê si̍t pun kàu chīn, só͘ tit-tio̍h ê siàu chiū-sī $23187$.
]

#example_equation(
    5fr,3fr,
[
#sp Siat-sú ū si̍t ê siàu $25930821$, beh ēng $36$ tsòe hoat lâi pun,
si̍t kap hoat pâi-lia̍t hó, khòaⁿ si̍t ê
tsó-pêng tio̍h tàu kàu saⁿ ê ūi chiū-sī
$259$, tsòe sin ê si̍t, chiah ū kàu thang
hō͘ hoat pun. $259$ ōe chiok $36$ chhit-#cc
pē. Ēng $7$ lâi sêng $36$, tit $252$, lâi kiám,
chhun $7$. Tùi goân ê si̍t chhú $3$, tàu $7$
tsòe $73$, ōe chiok $36$ nn̄g-pē. $2$ sêng $36$
tit $72$, lâi kiám, chhun $1$.Tùi goân ê
si̍t koh chhú $0$ tàu $1$ tsòe $10$, bô kàu
thang pun, kì $0$ tī tek-siàu, iū-koh chhú
$8$, tàu $10$ tsòe $108$, ōe chiok $36$ saⁿ-pē. $3$ sêng $36$ tit $108$, lâi
kiám, tùi-tú. Tùi goân ê si̍t koh chhú $2$, bô kàu thang pun, kì $0$ tī
tek-siàu, iū-koh chhú $1$, tàu $2$ tsòe $21$, iáu-kú bô kàu thang pun,
koh kì $0$ tī tek-siàu. Taⁿ goân ê si̍t pun kàu chīn, só͘ tit-tio̍h
ê siàu chiū-sī $7203004$, koh ū chhun lân-san $21$, bōe-pun-tit.
],
[$
36&)25930821(720300\
&#h(0.5em)underline(252)\
&#h(1.5em)73\
&#h(1.5em)underline(72)\
&#h(2.0em)108\
&#h(2.0em)underline(108)\
&#h(3.0em)21\
$],
)

#sp Kiám-chhái hoat siàu ê iū-pêng ū kúi-nā ūi ê $0$,
chiū thang *tiám-khui*, m̄-sái sǹg; tsóng-sī tī si̍t siàu
ê iū-pêng ia̍h tio̍h tiám-khui pîⁿ-pîⁿ hiah-ê ūi bián
sǹg, lō͘-bé tio̍h tàu chham tī só͘ chhun ê lân-san.

#example_equation(
    5fr,2fr,
[
#sp Siat-sú ū si̍t siàu $62534$, beh ēng $400$ tsòe
hoat lâi pun. Thang tiám-khui hoat ê iū-pêng
nn̄g ê khòng, chiū ia̍h tio̍h tiám-khui si̍t ê iū-#cc
pêng nn̄g ê ūi, chiū-sī $34$, chiah tio̍h ēng $4$ tsòe hoat lâi pun $625$,
chiū tit-tio̍h $156$; só͘ chhun ê $1$ tio̍h tàu $34$ tsòe $134$, chiū-sī
lân-san bōe pun-tit ê siàu.
],
[$
4 #tk 00&)underline(625 #tk 34)\
&156 dots.h.c 134
$],
)

#example_equation(
    4.5fr, 2.5fr,
[
#sp Koh siat-sú ū si̍t ê siàu $143654$, beh
ēng $25000$ tsòe hoat lâi pun. Thang tiám-#cc
khui hoat ê iū-pêng saⁿ ê $0$, chiū ia̍h tio̍h
tiám-khui si̍t ê iū-pêng saⁿ ê ūi, chiū-#cc
sī $654$, chiah tio̍h ēng $25$ tsòe hoat lâi pun $1434$, chiū tit-tio̍h $5$,
chhun $18$. Ēng $654$ tàu $18$ tsòe $18654$ ê lân-san bōe pun-tit-ê.
],
[$
25 #tk 000)&143 #tk 654(5\
&underline(125)\
&#h(0.5em)18654
$],
)

//p.26
#sp Kiám-chhái pun bōe-chīn, chiū só͘ chhun lân-san
ê *khia-liân*, thang siá tsòe *hūn-sò͘*, kì-lo̍h tī tek-siàu
ê iū-pêng.

#example_equation(
    5fr,2fr,
[
#sp Siat-sú beh pun $5437$ kho͘ hō͘ sì ê lâng. Pun
liáu chi̍t-lâng tit-tio̍h $1359$ kho͘, koh chi̍t-kho͘
sì-hūn ê chi̍t-hūn, chiū-sī nn̄g-kak-pòaⁿ.
],
[$
4)&underline(5437)\
&1359#scale(75%)[$1/4$]
$],
)
#v(0.5em,weak:true)
#example_equation(
    5fr,2fr,
[
#sp Koh, siat-sú beh pun $888$ tsòe $17$ hūn. Pun-#cc
liáu, ū chhun $4$. Chiū múi-hūn ū $52$, iā-koh $17$
hūn ê sì-hūn.
#v(0.5em,weak:true)
Phì-hūn siat-sú só͘ pun-ê chiū-sī $888$ pau tsóa,
koh múi-pau ū $17$ tiuⁿ. Pun hō͘ $17$ lâng, chiū
chi̍t lâng tit-tio̍h $52$ pau iā koh $4$ tiuⁿ.
],
[$
17)&888(52#scale(75%)[$4/17$]\
&underline(85)\
&#h(0.5em)38\
&#h(0.5em)underline(34)\
&#h(1.0em)4
$],
)

#sp Kiám-chhái tùi kiú-kiú-ha̍p-sò͘ tsai hoat-siàu
thang pun tsòe nn̄g-siàu, chiū thang chiong che nn̄g-#cc
tiâu-siàu liân-liân tsòe hoat lâi pun, chiàu ē-bīn
só͘ pâi-lia̍t ê hoat-tō͘.

#v(0.5em,weak:true)
#example_equation(
    5fr,2fr,
[
#sp Siat-sú ū si̍t-siàu $26901$ beh ēng $63$ lâi pun-i.
Tùi kiú-kiú-ha̍p-sò͘ tsai $63 = 7 times 9$ chiū tāi-seng
ēng $7$ tsòe hoat lâi pun tit-tio̍h $3843$; chiong
$3843$ tsòe sin ê si̍t, ēng $9$ tsòe hoat lâi pun-i tit-#cc
tio̍h $427$.
#v(0.5em,weak:true)
$427$ ēng $63$ koh sêng, chiū tit-tio̍h goân-siàu,
thang tsòe pîn-kù pun bô m̄ -tio̍h.
],
[$
7)underline(26901)&\
9)underline(3843)&\
427&\
63&\
overline(1281)&\
2562#h(0.5em)&\
overline(26901)&
$],
)

#v(0.5em,weak:true)
#sp Chiah ê siàu ha̍k-seng tio̍h sǹg; koh tek-siàu kap
hoat-siàu tio̍h siong-sêng, lâi tit-tio̍h si̍t-siàu, thang
tsòe pîn-kù pun bô m̄-tio̍h.

#example[
+ $1428 div 34$; $6882 div 31$; $26085 div 47$; $ 11934 div 51$.
+ $13892 div 234$; $15879237 div 79$; $971942910 div 97$.
+ $1567815 div 127$; $15954150 div 235$ $110880 div 504$.
+ Tio̍h chiàu nn̄g-ê hoat-tō͘ lâi sǹg $2252600 div 56$; $51851851380 div 42$; $45008946 div 81$; $1674 div 54$.
+ $446185740 div 19, 23, 77, 1011, 276, 3553, 7429, 19380, 25194, 1763580$.
+ $2016044352 div 672, 2016, 3000066, 336$.
+ $946847340 div 789, 23670, 1200060, 263$.
+ $16190955690 div 353, 4589, 16801, 100806, 32123, 504030$.
//p.27
#pagebreak()
#sp Téng-bīn chiah ê siàu pun ōe chīn, lóng bô chhun; ē-bīn,
kúi-nā tiâu pun-liáu ū chhun lân-san ê.
9. $34568135 div 357, 119, 714. 2142, 1428$.
10. $2700651897 div 2498, 76389, 32178, 98263$.
11. $123456789 div 20, 300, 4000, 50000, 6500, 71200, 89000, 90200$.
12. Ke-lāi só͘-hùi $25$ ji̍t chiū-sī $4700$ chîⁿ; chi̍t-ji̍t jōa-tsōe? #h(1fr)Tek $188$.
13. Tsa̍p-jī ge̍h-ji̍t chhù-sè $76200$ chîⁿ, chiū chi̍t-ge̍h-ji̍t jōa tsōe?
#v(0.5em,weak:true)
#h(1fr)Tek $6350$.
14. Siat-sú khui hé-hun-chhia-lō͘ $360$ lí, só͘-hùi tio̍h $3024360$ kho͘, chiū khui chi̍t-lí só͘-hùi tio̍h kúi-kho͘? #h(1fr)Tek $8401$.
15. Chi̍t-kun $16$ niú, chiū $7376$ niú kúi-kun? #h(1fr)Tek $461$.
16. Si $46$ kun chit-toān $996$ chhioh, chiū múi-kun chit kúi-chhioh?
#v(0.5em,weak:true)
#h(1fr)Tek $21$.
17. Chi̍t-ji̍t $24$ tiám-cheng; chi̍t-ji̍t ia̍h $96$ khek; chi̍t-ji̍t ia̍h $1440$ hun; chi̍t-ji̍t ia̍h $86400$ biáu: taⁿ mn̄g, chi̍t-tiám-cheng kúi-khek? Kúi-hun? Kúi-biáu? Koh chi̍t-khek kúi-hun? Kúi-biáu?
#v(0.5em,weak:true)
#h(1fr)Tek $4$ khek; $60$ hun; $3600$ biáu; $15$ hun; $900$ biáu.
18. Bó͘-lâng *kè-óng* lâu $3000$ kho͘ hō͘ bó͘-kiáⁿ; bó͘ tit-tio̍h saⁿ-hūn-chi̍t, só͘ chhun ê pîⁿ-pîⁿ pun hō͘ kiáⁿ-jî poeh lâng: taⁿ mn̄g bó͘ tit-tio̍h jōa-tsōe? kiáⁿ-jî ta̍k-lâng jōa-tsōe? Bó͘ $1000$ kho͘; Kiáⁿ-jî *sûi-lâng* $250$ kho͘.
19. Chi̍t-nî $365$ ji̍t; chi̍t-ji̍t $24$ tiám-cheng; chi̍t-tiám-cheng $60$ hun; chi̍t-hun $60$ biáu: siat-sú siōng-kūn ê chhiⁿ lī tōe $78225678720000$ lí, koh i-ê kng tio̍h kiâⁿ $4$ nî kú chiah ōe kàu tōe; taⁿ mn̄g, chi̍t-biáu kú, kng ōe kiâⁿ kúi-lí? #h(1fr)Tek $620130$.
20. Tōa-chhèng ê chi̍t-lia̍p-chí, chi̍t-biáu kú ōe kiâⁿ chi̍t-lí, chiū tio̍h kiâⁿ kúi-nî chiah ōe kàu siōng-kūn ê chhiⁿ? #h(1fr)Tek $2480520$.
21. Sǹg $1$ kho͘ $1080$ chîⁿ, chiū pun $3$ kho͘ hō͘ $8$ lâng, ta̍k-lâng tit-tio̍h kúi ê îⁿ? #h(1fr)Tek $405$.
22. Bó͘ chhù-tsú sè $16$ keng chhù, chiū $1$ nî niá $96$ kho͘. Sǹg chi̍t-kho͘ $1100$ chîⁿ, chi̍t-keng ge̍h-ge̍h ê chhù-sè, kúi ê-îⁿ? #h(1fr)Tek $550$.
23. $5$ lâng tsòe kang $6$ ji̍t kú, kiōng thàn $10$ kho͘. Sǹg chi̍t kho͘ $1080$ chîⁿ, chiū chi̍t lâng chi̍t-ji̍t thàn kúi ê-îⁿ? #h(1fr)Tek $360$.
24. Chi̍t-tàⁿ han-tsû ta̍t $340$ chîⁿ; saⁿ-khu han-tsû-hn̂g, lóng pîⁿ-tōa, só͘-siⁿ ê han-tsû, kiōng ta̍t $68$ kho͘. Sǹg chi̍t-kho͘ $1080$ chîⁿ chiū chi̍t khu kúi tàⁿ? #h(1fr)Tek $72$.
]
