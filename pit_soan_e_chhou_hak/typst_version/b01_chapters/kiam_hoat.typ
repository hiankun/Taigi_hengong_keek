#import "../chheh.typ": chheh, example, example_equation, sp, cc

= KIÁM-HOAT.

#sp Lâi pí-phēng nn̄g tiâu-siàu, ēng khah-sòe-tiâu
khàu-tû khah tōa-tiâu khòaⁿ ū chhun jōa-tsōe, hō
tsòe *Kiám-hoat*. Kiám-hoat Ka-hoat *saⁿ-tùi-hoán*.

Khah-tōa-tiâu-siàu tio̍h thán-hoâiⁿ siá tsòe *téng-#cc
lia̍t*, khah-sòe-tiâu tio̍h hoâiⁿ-lia̍t tī ē-bīn; toaⁿ ê ūi
tùi toaⁿ ê ūi, tsa̍p ê ūi tùi tsa̍p ê ūi, í-siōng kok tùi
i ê ūi pâi. Pâi-lia̍t bêng-pe̍k, tio̍h u̍ih-hûn keh-kài;
tāi-seng tùi toaⁿ ê ūi kiám-khí, ēng *ē-siàu* kiám
*téng-siàu*, khòaⁿ chhun jōa-tsōe, chiū kì tī pún-ūi ê
hûn-ē. Kok-ūi tio̍h chiàu chit-ê hoat-tō͘. 

#example_equation(
    7fr,1fr,
[
#sp Beh tùi $3897$ lâi kiám $1326$, khòaⁿ kai chhun jōa-tsōe; tio̍h
chiong tōa siàu siá téng-bīn lia̍t, sió siàu siá ē-tóe lia̍t,
u̍ih-hûn keh-kài; khòaⁿ toaⁿ-ūi ê $7$ kiám $6$ sīn $1$, tio̍h
kì $1$ tī pún-ūi hûn-ē; tsa̍p ūi ê $9$ kiám $2$ sīn $7$, pah-#cc
ūi ê $8$ kiám $3$ sīn $5$, chheng-ūi ê $3$ kiám $1$ sīn $2$, kok
tùi pún-ūi ê hûn-ē lâi kì, chiū tsai tōa siàu só͘ chhun
chiū-sī $2571$.
],
[$
3897&\
underline(1326)&\
2571&
$],
)

//p.12
#example[
#sp Ha̍k-seng teh-sǹg ê sî, tio̍h ho͘ kóng, $7$ kiám $6$, sīn $1$ kì $1$.
$9$ kiám $2$, sīn $7$, kì $7$. $8$ kiám $3$, sīn $5$, kì $5$. $3$ kiám $1$, sīn $2$, kì $2$.
]

Ē-bīn chiah ê siàu tio̍h sûi-tiâu kiám khòaⁿ chhun jōa-tsōe.

#example[
+ $5$ kiám $3$; $6$ kiám $4$; $7$ kiám $4$; $8$ kiám $3$; $9$ kiám $5$; $11$ kiám $8$; $12$ kiám $8$.
+ $23$ kiám $11$; $34$ kiám $12$; $57$ kiám $24$; $68$ kiám $34$; $96$ kiám $51$; $95$ kiám $91$; $86$ kiám $85$.
+ Tùi $364$ kiám $222$; tùi $607$ kiám $401$; tùi $670$ kiám $560$; tùi $43105$ kiám $21003$.
+ Só͘ pâi-lia̍t ê siàu tio̍h kiám: #grid(columns:(1fr,1fr,1fr,1fr,1fr),
    rows:(auto),
[$
7649&\
underline(4227)&
$],
[$
89546&\
underline(73210)&
$],
[$
97600&\
underline(35200)&
$],
[$
86976454&\
underline(75716254)&
$],
[$
64375&\
underline(33333)&
$],
)
]

#sp Kiám-chhái ē-lia̍t ê ūi ū siàu khah tōa téng-lia̍t
saⁿ-tùi ê ūi, bô kàu-gia̍h thang kiám, chiū tio̍h tùi
chêng-ūi *chioh-chi̍t hòa tsa̍p* lâi thiⁿ pún-ūi téng-lia̍t
ê siàu, jiân-āu lâi kiám; só͘-chhun-ê kì lok pún-ūi ê
hûn-ē, tùi chêng-ūi só͘ chioh ê chi̍t tio̍h hêng lâi thiⁿ
chêng-ūi ê ē-lia̍t.

#example_equation(
    7fr,1fr,
[
#sp Tùi $18629$ kiám $9573$; khòaⁿ kai chhun jōa-tsōe;
tio̍h chiàu hoat-tō͘ pâi-lia̍t, u̍ih-hûn keh-kài; khòaⁿ
toaⁿ-ūi ê $9$ kiám $3$, sīn $6$, tio̍h kì $6$ tī pún-ūi ê hûn-ē,
tsa̍p ūi ê $2$ kiám $7$, bô kàu kiám, tio̍h tùi chêng-ūi
chioh chi̍t hòa tsa̍p, thiⁿ chham tsòe $12$, chiū sīn $5$,
lâi kì $5$ lok hûn-ē; só͘ chioh ê chi̍t tio̍h thiⁿ hêng chêng-ūi ê $5$
tsòe $6$; taⁿ $6$ kiám $6$ sī *tùi-tú*, tio̍h kì $0$ lok hûn-ē; $8$ kiám $9$,
bô kàu kiám tio̍h tùi chêng-ūi chioh chi̍t hòa tsa̍p thiⁿ-chham
tsòe $18$, chiū sīn $9$, lâi kì $9$ lok hûn-ē; só͘ chioh ê chi̍t tio̍h thiⁿ
hêng chêng-ūi ê ē lia̍t tsòe $1$; taⁿ $1$ kiám $1$ sī tùi-tú, tio̍h kì $0$
lok hûn-ē, nā-sī chīn-thâu ê ūi kì $0$ bô lō͘-ēng, chiah m̄-sái kì:
chiū tsai só͘ chhun chiū-sī $9056$.
],
[$
#underline(offset:1.5em,[18629])&\
9573&\
9056&
$],
)
#example[
#sp Ha̍k-seng teh-sǹg ê sî, tio̍h ho͘ kóng, $9$ kiám $3$, sīn $6$, kì $6$. $2$
kiám $7$, bô kàu kiám, chioh $10$, $12$ kiám $7$, sīn $5$, kì $5$. $5$ hoân
$1$ tsòe $6$. $6$ kiám $6$ tùi-tú, kì $0$. $8$ kiám $9$, bô kàu kiám, chioh
$10$, $18$ kiám $9$, sīn $9$, kì $9$. Hoân $1$, $1$ kiám $1$, tùi-tú, m̄-sái kì.
]

#pagebreak()
//p.13
#sp Chit-ê chioh koh hêng ê hong-hoat, kiám-chhái
ū lâng bōe bêng-pe̍k, chiū tio̍h siūⁿ tùi chêng-ūi
chioh chi̍t chiū hit-ūi tio̍h khah chió, nā-sī bô hō͘ i
khah-chió, sòa thiⁿ ē-lia̍t chiū-sī saⁿ-tâng.

#example_equation(
    7fr,1fr,
[
#sp Khòaⁿ piⁿ-thâu só͘ pâi-lia̍t ê siàu. $5$ kiám $6$ bô
thang kiám, tùi chêng-ūi ê $4$ (chiū-sī $4$ tsa̍p) chioh
$1$ tsa̍p lâi thiⁿ-chham tsòe $15$ kiám $6$ chhun $9$. Taⁿ
tùi tsa̍p ūi ê $4$ ū chioh $1$ tio̍h chhun $3$, $3$ kiám $2$
chhun $1$. Nā-sī khòaⁿ téng-lia̍t iû-goân tsòe $4$, lâi
thiⁿ ē-lia̍t ê tsa̍p-ūi tsòe $3$; $4$ kiám $3$ chhun $1$, iā-sī án-ni. Chit-#cc
ê hoat-tō͘ khah hó in-ūi kiám téng-lia̍t khah-ōe chhò-gō͘. Só͘-í
siat chit-ê chioh-hêng ê hoat-tō͘. 
],
[$
4345&\
underline(2826)&\
1519&
$],
)
#v(0.5em,weak:true)
#example_equation(
    7fr,1fr,
[
#sp Koh khòaⁿ piⁿ-thâu só͘ pâi-lia̍t ê siàu chiū kiám-#cc
chhái ōe khah bêng.
#v(0.5em,weak:true)
$4$ kiám $2$ chhun $2$. Téng-lia̍t ē-lia̍t pîⁿ-pîⁿ tio̍h
thiⁿ $10$, tsòe $14$, $12$, iû-goân chhun $2$.
#v(0.5em,weak:true)
Taⁿ tio̍h koh khòaⁿ pún-siàu.
],
[
    #grid(
        columns: (1fr,1fr),
        rows: (auto),
        $
        4&\
        underline(2)&\
        2&
        $,
        $
        14&\
        underline(12)&\
        2&
        $,
    )
],
)


#example_equation(
    7fr,1fr,
[
#sp $5$ kiám $6$ bô thang kiám, chiū téng-lia̍t tio̍h thiⁿ
$10$, tsòe $15$. Lóng m̄-sái kóng chioh, iā m̄-sái gī-lūn
hit-ê $10$ tùi tah-lo̍h lâi. kan-ta thiⁿ-chham $10$ tiāⁿ-tiāⁿ.
Kiám $6$ chhun $9$. Taⁿ in-ūi téng-lia̍t í-keng ū thiⁿ-#cc
chham $10$, chiū ē-lia̍t iā tio̍h thiⁿ $10$, tsóng-sī tsa̍p-ūi
lâi thiⁿ, tio̍h thiⁿ $1$ tiāⁿ-tiāⁿ (chiū-sī $1$ tsa̍p), $2$ thiⁿ $1$ kiōng $3$.
$4$ kiám $3$ chhun $1$. $3$ (chiū-sī $3$ pah) kiám $8$ (chiū-sī $8$ pah) bô
thang kiám chiū téng-lia̍t thiⁿ-chham $10$ tsòe $13$ (chiū-sī $13$
pah) kiám $8$ chhun $5$ (chiū-sī $5$ pah). Ē-lia̍t thiⁿ $10$ ($10$ pah
chiū-sī $1$ chheng). $2$ thiⁿ $1$ kiōng $3$. $4$ kiám $3$ chhun $1$.
],
[$
4345&\
underline(2826)&\
1519&
$],
)

#sp Chiah ê siàu tio̍h pâi-lia̍t lâi sǹg.

#example[
+ Tùi $42134$ lâi kiám $18765$; tùi $46622$ lâi kiám $38742$; tùi $40000$ lâi kiám $20040$.
+ Góa khiàm lâng $1860$ chîⁿ, chiū hō͘-i chi̍t-kho͘ gûn ōe ōaⁿ-tit $1190$ chîⁿ: tio̍h koh hō͘-i chîⁿ jōa-tsōe?
+ Lâng ū chîⁿ $11345$ chiū the̍h-chhut $9203$: iáu chhun jōa-tsōe-mah?
+ Só͘ pâi-lia̍t ê siàu tio̍h khàu-tû khòaⁿ si̍t jōa-tsōe:  #grid(columns:(1fr,1fr,1fr,1fr,1fr),
    rows:(auto),
[$
#underline(offset:1.5em,[135876])&\
96789&
$],
[$
#underline(offset:1.5em,[10000])&\
9999&
$],
[$
#underline(offset:1.5em,[101010])&\
89899&
$],
[$
#underline(offset:1.5em,[101001000])&\
99999999&
$],
[$
#underline(offset:1.5em,[101])&\
11&
$],
)
//p.14
+ Tùi chi̍t-bān lâi khàu-tû tsa̍p-ê; tùi tsa̍p-ek lâi khàu-tû chi̍t-ê; tùi tsa̍p-ek liân tsa̍p bān lâi khàu-tû káu ek káu chheng liân káu-tsa̍p-káu.
+ Tùi chi̍t-pah-bān liân tsa̍p-i̍t khàu-tû káu-pah liân káu-ê; Tùi jī-tsa̍p-gō͘-bān khàu-tû jī-tsa̍p-sì-bān liân tsa̍p-ê; Tùi tsa̍p tiāu khàu-tû tsa̍p-ek.
+ Hâm-hong goân-nî chiū-sī *Kiù-tsú kàng-seng* $1851$ nî, Tông-tī goân-nî chiū-sī $1862$. Mn̄g Hâm-hong *chē-ūi* kúi-nît?
+ Góa khì bó͘ lâng ê tiàm-thâu bóe pò͘, ta̍t chîⁿ $867$, chiū hō͘-i chi̍t-kho͘-gûn thang ōaⁿ $1113$ chîⁿ, i tio̍h tsāu góa jōa-tsōe chîⁿ?
+ Tāi-chheng thâu-chi̍t-ê hông-tè, Sūn-tī goân-nî chiū-sī Kiù-tsù kàng-seng $1644$ nî; tùi hit-sî kàu-taⁿ kúi-nî?
+ Ēng $123$, $456$, $789$ lâi siong-ka, tùi só͘ tit-tio̍h ê tsóng-siàu khàu-tû $658$: iáu chhun jōa-tsōe?
+ Bó͘-lâng chi̍t-ji̍t ū thàn $125$ chîⁿ, koh chi̍t-ji̍t ū thàn $150$, iū koh chi̍t-ji̍t ū thàn $186$, chiū ēng $225$ chîⁿ lâi *tia̍h bí*: Iáu chhun jōa-tsōe?  #h(1fr)Tek $236$ chîⁿ.
+ Tùi $1100010$ tio̍h khàu-tû $123456$, $654321$ ê tsóng-siàu, khòaⁿ chhun jōa-tsōe. #h(1fr)Tek $322233$.
+ Tsa̍p-poeh séng ê lâng-gia̍h chiū-sī téng-bīn Ka-hoat hit-phiⁿ só͘ kì-tsài: Taⁿ mn̄g, Hok-kiàn chió Sù-chhoan jōa-tsōe lâng?; Kńg-tang chió Hok-kiàn jōa-tsōe lâng?; Hûn-lâm Kùi-chiu ha̍p-kiōng chió Kńg-tang jōa-tsōe?; Chiat-kang chió Ti̍t-lē jōa-tsōe? Ô͘-lâm Kńg-tang ha̍p-kiōng chió Ô͘-pak jōa-tsōe? Kang-so͘ chió Ô͘-lâm chham Ô͘-pak jōa-tsōe?
+ Bó͘ lâng tsòe seng-lí, thâu-chi̍t nî ū thàn $300$ niú, tē-jī nî ū thàn $485$ niú, tē-saⁿ nî ū *si̍h* $277$ niú, tē-sì nî ū thàn $1230$ niú, tē-gō͘ nî ū si̍h $820$ niú, tē-la̍k nî ū thàn $3566$ niú: Taⁿ mn̄g, Chí la̍k-nî-lāi siōng[kiōng?] thàn jōa-tsōe; #h(1fr)Tek $4484$ niú.
+ Nn̄g-lâng Tsoân-chiu khí-sin kiâⁿ siâng-lō͘, chi̍t-lâng khiâ-bé, chi̍t-lâng pō͘-kiâⁿ; khiâ-bé-ê ta̍k-ji̍t kiâⁿ $95$ lí, pō͘-kiâⁿ-ê ta̍k-ji̍t kiâⁿ $66$ lí: Ke[Kè?] saⁿ-ji̍t sûi-lâng ū kiâⁿ kúi-lí? Koh saⁿ-lī kúi-lí? #h(1fr)Tek $285$, $198$, $87$.
+ Ū lâng thàn chîⁿ $561$, nā-sī hit-jit[ji̍t?] só͘-hùi chiū-sī tia̍h-bí $200$, bóe-bah $100$, bóe-chhài $30$, bóe-chhâ $20$, bóe-tāu-koaⁿ $15$, bóe-tāu-iû $5$, taⁿ-tsúi $12$, *tah-hé-iû* $24$: Iáu chhun jōa-tsōe? #h(1fr)Tek $155$.
+ Ē-mn̂g kàu Ngó͘-thong $30$ lí lō͘, Ngó͘-thong kàu La̍k-gō͘-tàiⁿ $25$ lí, La̍k-gō͘-tàiⁿ kàu Tōa-iâⁿ $68$ lí, Tōa-iâⁿ kàu Gō͘-lêng $35$ lí, Gō͘-lêng kàu Tsoân-chiu $33$ lí. Kiám-chhái lâng Ē-mn̂g khí-sin, khùn Tōa-iâⁿ, nn̄g-ji̍t ê lō͘ *tsoa̍h* kúi-lí? #h(1fr)Tek $55$.
]

