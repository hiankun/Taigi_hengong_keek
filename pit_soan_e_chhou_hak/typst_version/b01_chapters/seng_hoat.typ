#import "../chheh.typ": chheh, example, example_equation, sp, cc

//p.15
= SÊNG-HOAT.

#sp Sêng-hoat kap Ka-hoat *Siāng-lūi*. Kiám-chhái ū
kúi-nā tiâu siàu saⁿ-tâng-ê, beh ha̍p-kiōng tsòe chi̍t-#cc
tiâu, Ka-hoat khah *hùi-khì*, sêng-hoat khah *kín-khoài*.

#grid(
columns: (1fr,6fr,1fr),
rows: (auto),
example[$
123&\
123&\
underline(123)&\
369&
$],
example[
#sp Siat-sú ū saⁿ tiâu ê $123$, Kè-kiōng jōa-tsōe?
Chiàu sêng-hoat sǹg tio̍h kóng $3$ ê $3$ chiū-sī $9$.
$3$ ê $2$ chiū-sī $6$. $3$ ê $1$ chiū-sī $3$.
#v(0.5em,weak:true)
Ka-hoat pâi-lia̍t tī tò-chhiú-pêng, sêng-hoat
tī chiàⁿ-chhiú-pêng; Ha̍k-seng it-bo̍k liáu-jiân.
],
example[$
#underline(offset:1.5em,[123])&\
3&\
369&
$],
)

#sp Sêng-hoat iàu-kín tio̍h ha̍k-si̍p *Kiú-kiú-ha̍p-sò͘*,
só͘-í ē-bīn ū pâi-lia̍t, hō͘ ha̍k-seng thang liām kàu-#cc
sek.

#example[
#table(
    columns: (1fr,1fr,1fr,1fr,1fr,1fr,1fr,1fr,1fr,1fr,1fr,1fr),
    align: center,
    [*$1$*],[*$2$*],[*$3$*],[*$4$*],[*$5$*],[*$6$*],[*$7$*],[*$8$*],[*$9$*],[*$10$*],[*$11$*],[*$12$*],
    [*$2$*],[$4$],[$6$],[$8$],[$10$],[$12$],[$14$],[$16$],[$18$],[$20$],[$22$],[$24$],
    [*$3$*],[$6$],[$9$],[$12$],[$15$],[$18$],[$21$],[$24$],[$27$],[$30$],[$33$],[$36$],
    [*$4$*],[$8$],[$12$],[$16$],[$20$],[$24$],[$28$],[$32$],[$36$],[$40$],[$44$],[$48$],
    [*$5$*],[$10$],[$15$],[$20$],[$25$],[$30$],[$35$],[$40$],[$45$],[$50$],[$55$],[$60$],
    [*$6$*],[$12$],[$18$],[$24$],[$30$],[$36$],[$42$],[$48$],[$54$],[$60$],[$66$],[$72$],
    [*$7$*],[$14$],[$21$],[$28$],[$35$],[$42$],[$49$],[$56$],[$64$],[$70$],[$77$],[$84$],
    [*$8$*],[$16$],[$24$],[$32$],[$40$],[$48$],[$56$],[$64$],[$72$],[$80$],[$88$],[$96$],
    [*$9$*],[$18$],[$27$],[$36$],[$45$],[$54$],[$63$],[$72$],[$81$],[$90$],[$99$],[$108$],
    [*$10$*],[$20$],[$30$],[$40$],[$50$],[$60$],[$70$],[$80$],[$90$],[$100$],[$110$],[$120$],
    [*$11$*],[$22$],[$33$],[$44$],[$55$],[$66$],[$77$],[$88$],[$99$],[$110$],[$121$],[$132$],
    [*$12$*],[$24$],[$36$],[$48$],[$60$],[$72$],[$84$],[$96$],[$108$],[$120$],[$132$],[$144$],
)
]

#sp Sêng-hoat, tio̍h ēng *pún-siàu* kiò-tsòe *si̍t*, *sêng-#cc
siàu* kiò-tsòe *hoat*.
#v(0.5em,weak:true)
Khòaⁿ Kiú-kiú ha̍p-sò͘ chiū tsai $6$ ê $8$ kap $8$ ê $6$
saⁿ-tâng $48$, khó-kiàn chiong hoat tsòe si̍t, si̍t tsòe
hoat iā thang, siong-sêng só͘ tit ê siàu bô *chêng[cheng?]-tsoa̍h*.
#v(0.5em,weak:true)
Kiám-chhái ū nn̄g-tiâu siàu, chi̍t-tiâu khah-tsōe-#cc
//p.16
ūi, chi̍t-tiâu khah-chió, chiū tio̍h chiong khah-tsōe
ūi-ê, siá tī téng-lia̍t tsòe si̍t, khah-chió-ê, siá tī ē-#cc
bīn tsòe hoat, u̍ih-hûn keh-kài. Sêng-khí tio̍h tùi
si̍t ê toaⁿ-űi sêng-liáu só͘ tit ê siàu hit-ê lân-san
tio̍h kì hûn-ē ê toaⁿ-ūi, chiūⁿ-tsa̍p-ê tio̍h chìn chêng-#cc
ūi, chhin-chhiūⁿ Ka-hoat chi̍t-poaⁿ; í-siōng *it-ûn* tio̍h
án-ni.
#v(0.5em,weak:true)
Kiám-chhái hoat nā ū chi̍t-ūi tiāⁿ-tiāⁿ, chiū tio̍h
siá tī si̍t ê toaⁿ-ūi-ē u̍ih-hûn keh-kài, tùi si̍t ê tōaⁿ[toaⁿ?]-#cc
ūi sêng-khí.

#example_equation(
    7fr, 1fr,
[ 
#sp Siat-sú ū pún siàu $592$, beh ēng $4$ lâi sêng-i, khòaⁿ kai tit
jōa-tsōe. Hoat-tō͘ tio̍h chhin-chhiūⁿ piⁿ-thâu só͘-siá.
$592$ tio̍h tsòe si̍t, $4$ tio̍h tsòe hoat, u̍ih-hûn keh-kài.
Ēng $4$ sêng $2$ tit $8$, kì $8$ tī hûn-ē ê toaⁿ-ūi. $4$ sêng $9$
tit $36$, kì $6$ tī hûn-ē ê tsa̍p-ūi, chìn $3$. $4$ sêng $5$ tit $20$,
kap só͘ chìn ê $3$, kiōng $23$, tio̍h kì $23$ tī hûn-ē. Taⁿ
só͘ tit-tio̍h chiū-sī $2368$.
],
[$
592&\
4&\
#line(length:2em)&\
2368&
$],
)
#v(0.5em,weak:true)
#example[
#sp Ha̍k-seng tio̍h ho͘ kóng, $4$, $2$ *jû* $8$, kì $8$. $4$, $9$ jû $36$, kì $6$, chìn
$3$. $4$, $5$ jû $20$ kiōng $3$, $23$, kì $23$.
]
#v(0.5em,weak:true)
#example_equation(
    7fr, 1fr,
[ 
#sp Koh piⁿ-thâu pâi-lia̍t siàu hō͘ ha̍k-seng thang
ho͘ lâi sǹg. Tio̍h ho͘ kóng $6$, $5$ jû $30$, kì $0$, chìn
$3$. $6$, $1$ jû $6$ *kiōng* $3$, $9$, kì $9$. $6$, $3$ jû $18$, kì $8$, chìn
$1$. $6$, $4$ jû $24$, kiōng $1$, $25$, kì $5$, chìn $2$. $6$, $2$ jû $12$,
kiōng $2$, $14$, kì $14$.
],
[$
24315&\
6&\
#line(length:3em)&\
145890&
$],
)

#sp Chiah-ê siàu ha̍k-seng tio̍h ho͘ lâi sǹg.

#example[
+ $1234 times 2$; $230156 times 3$; $610378 times 4$; $867654 times 5$; $635432 times 6$; $653002 times 8$; $8910046875 times 9$.
+ Tia̍h-bí, chi̍t-táu tio̍h $285$ chîⁿ, chiū la̍k táu tio̍h jōa-tsōe?
+ Chi̍t-tè *phia̍h* ta̍t chhit-ê-îⁿ, chiū $3493$ ta̍t jōa-tsōe?
+ Chi̍t-kho͘-gûn ōe-ōaⁿ-tit $1125$ chîⁿ, chiū $8$ kho͘ tio̍h ōaⁿ jōa-tsōe?
+ Tsûn tsài thô͘-thòaⁿ, chi̍t-*tsāi* $23460$ tàⁿ; chhit-tsāi tio̍h kúi tàⁿ?
+ Chi̍t-lâng chi̍t-ji̍t ê kang tio̍h $125$ chîⁿ, chiū la̍k-lâng la̍k-ji̍t ê kang tio̍h jōa-tsōe-chîⁿ? #h(1fr)Tek $4500$.
+ Siat-sú chi̍t-tè-tsng ta̍t-tio̍h la̍k-ê-îⁿ, koh $125$ tè ōe pho͘ *chi̍t-tn̄g sù-hong*: taⁿ beh pho͘ tiâⁿ, it-bīn káu-tn̄g it-bīn chi̍t-tn̄g, tio̍h kúi-tè-tsng? iā tio̍h jōa-tsōe-chîⁿ? #h(1fr) Tek $1125$; $6750$.
//p.17
+ Chi̍t-ia̍h-chheh chi̍t-pêng káu-tsōa, chi̍t-tsōa jī-tsa̍p-sì jī: Poeh-ia̍h lóng-kiōng jōa-tsōe jī? #h(1fr)Tek $3456$.
]

#sp Kiám-chhái hoat-siàu ū kúi-nā ūi, chiū tio̍h pâi-#cc
lia̍t tī si̍t-siàu-ē, hō͘ toaⁿ tùi toaⁿ, tsa̍p tùi tsa̍p í-#cc
siōng sûi-ūi saⁿ-tùi; chiū tio̍h chiong hoat ê toaⁿ-ūi
ê siàu lâi sêng si̍t-siàu, kì só͘ tit-tio̍h ê siàu tī-hûn-#cc
ē, kiò i tsòe *tē-it-lia̍t*. Koh tio̍h ēng hoat ê tsa̍p-ūi
ê siàu chiàu-kū sêng si̍t-siàu, kì só͘ tit-tio̍h tsòe tē-#cc
jī-lia̍t, tsóng-sī in-ūi chit-lia̍t bô toaⁿ-ūi chiū tio̍h tī
téng-lia̍t ê tsa̍p-ūi-ē *khí-sin-kì*. Ēng hoat ê pah-ūi
ê siàu lâi sêng iā-sī an[án]-ni; só͘ tit-tio̍h ê siàu tio̍h
tsòe tē-saⁿ-lia̍t, iā tio̍h tùi pah ūi kì-khí: í-siōng lóng
chiàu chit-ê hoat-tō͘.
#v(0.5em,weak:true)
Bêng-pe̍k tio̍h koh u̍ih-hûn, chiàu Ka-hoat lâi
*kiat*, kì tī hûn-ē, só͘ tit-tio̍h chiū-sī tsóng-siàu.

#example_equation(
    6.5fr,1.5fr,
[
#sp Siat-sú ū pún-siàu $1678$, beh ēng $243$ lâi sêng-i, chiū tio̍h siá
$1678$ tsòe si̍t, $243$ tsòe hoat u̍ih-hûn, ēng hoat toaⁿ-#cc
ūi ê $3$ lâi sêng hit-ê si̍t, só͘ tit-tio̍h ê $5034$ tio̍h siá
hûn-ē tsòe tē-it-lia̍t. Koh tio̍h ēng hoat tsa̍p-ūi ê
$4$ lâi sêng hit ê si̍t, só͘ tit-tio̍h ê $6712$ tio̍h siá tsòe
tē-jī lia̍t, tsóng-sī in-ūi $2$ si̍t-tsāi sī $20$ tio̍h siá tī
tsa̍p-ūi, $1$ tī pah-ūi í-siōng lóng chiàu chit ê hoat-#cc
tō͘. Koh tio̍h ēng hoat pah-ūi ê $2$ lâi sêng hit ê
si̍t. $2$, $8$ jû $16$ kì $6$ chìn $1$, tsóng-sī chit-ê $16$ si̍t-#cc
tsāi sī $16$ pah só͘-í tio̍h kì $6$ tī pah-ūi, í-siōng lóng
sêng-liáu, só͘ tit-tio̍h ê $3356$ tio̍h tsòe tē saⁿ-lia̍t: koh u̍ih-hûn,
só͘ tit-tio̍h saⁿ-tiâu-siàu tio̍h chiàu ka-hoat lâi kiat; só͘ tit-tio̍h
ê $407754$ chiū-sī tsóng-siàu.
],
[$
1678&\
243&\
#line(length:3em)&\
5034&\
6712#h(0.5em)&\
3356#h(1em)&\
#line(length:3em)&\
407754&
$],
)
#v(0.5em,weak:true)
#example_equation(
    6.5fr,2.5fr,
[
#sp Siat-sú beh ēng $140503$ lâi sêng
$45000726$. Pâi-lia̍t, u̍ih-hûn bêng-pe̍k, tio̍h
ēng $3$ lâi sêng hit-ê si̍t, kì só͘ tit-tio̍h ê siàu
tsòe tē-it lia̍t. Tsa̍p-ūi ê $0$ *sêng bô*, tsóng-#cc
sī tio̍h tī tē-it lia̍t ê tsa̍p-ūi-ē kì $0$ thang
lâu i-ê ūi, koh tio̍h ēng pah-ūi ê $5$ lâi sêng
hit-ê si̍t, só͘ tit-tio̍h ê siàu kì tsòe tē-jī-lia̍t,
tsóng-sī tio̍h tùi pah-ūi kì-khí. Chheng-ūi ê
//p.18
$0$ sêng bô, nā-sī tio̍h kì $0$ lâi lâu i-ê ūi; ēng bān ūi ê $4$ lâi sêng, só͘
tit-tio̍h kap tú-á só͘ kì ê $0$ tsòe tē-saⁿ-lia̍t. Tsa̍p-bān ê ūi ê $1$ lâi
sêng, só͘ tit-tio̍h *siú* tsòe tē-sì lia̍t, tùi tsa̍p-bān ê ūi kì-khí; u̍ih-#cc
hûn lâi kiat, só͘ tit-tio̍h ê tsóng-siàu chiū-sī $6322737005178$.
],
[$
45000726&\
140503&\
#line(length:6.5em)&\
135002178&\
2250036300#h(0.5em)&\
1800029040#h(1.5em)&\
45000726#h(2.5em)&\
#line(length:6.5em)&\
6322737005178&
$],
)

#sp Kiám-chhái hoat-siàu iū-pêng-thâu ê ūi ū khòng,
put-koán kúi-ūi, lóng m̄-sái sêng, nā-tio̍h chiong ū
siàu ê ūi lâi siong-sêng; siong-sêng bêng-pe̍k chiah
tio̍h kì só͘ ū ê khòng tī hûn-ē tsóng-siàu ê iū-pêng..

#example[
Só͘ pâi-lia̍t, saⁿ-tiâu siàu thang *tsòe iūⁿ*.

#grid(
    columns: (1fr,1fr,1fr),
    rows:(auto),
    [$
    368#h(1em)&\
    2400&\
    #line(length:3em)&\
    147200&\
    736#h(1.5em)&\
    #line(length:3em)&\
    883200&
    $],
    [$
    465#h(1.5em)&\
    32000&\
    #line(length:4em)&\
    930000&\
    1395#h(2em)&\
    #line(length:4em)&\
    14880000&
    $],
    [$
    360#h(1.5em)&\
    14000&\
    #line(length:3.5em)&\
    1440000&\
    360#h(2em)&\
    #line(length:3.5em)&\
    5040000&
    $],
)
]

#v(2em) //FIX
#sp Kiám-chhái tùi Kiú-kiú-ha̍p-sò͘ tsai hoat-siàu
tsòe nn̄g-tiâu-siàu siong-sêng só͘-siⁿ-ê, chiū thang
chiong che nn̄g-tiâu liân-liân tsòe hoat, chiàu ē-tóe
só͘ pâi-lia̍t ê hoat-tō͘.

#example_equation(
    5.0fr,2.0fr,
[
#sp Siat-sú beh ēng $42$ lâi sêng $37218$. Tùi kiú-kiú-ha̍p-sò͘ thang
tsai $6 times 7=42$, chiū tio̍h chiong $37218$ lia̍t
tī téng-bīn tsòe si̍t.
#v(0.5em,weak:true)
Tāi-seng ēng $6$ tsòe hoat lâi sêng, só͘ tit-#cc
tio̍h ê siàu kì tī hûn-ē. Koh tio̍h chiong só͘
tit-tio̍h ê siàu tsòe sin ê si̍t, ēng $7$ lâi sêng-#cc
i. Só͘ tit-tio̍h ê siàu chiū-sī tsóng-siàu.
#v(0.5em,weak:true)
Tú-á só͘ sǹg ê siàu ha̍k-seng tio̍h koh
sǹg, ēng $7$ tsòe thâu chi̍t-ê hoat-siàu, ēng
$6$ tsòe tē-jī: iā tio̍h koh ēng $42$ chiàu pêng-siông ê hoat-tō͘ lâi
sêng.
],
[$
37218&\
6& times 7=42\
#line(length:3.5em)&\
223308&\
7&\
#line(length:3.5em)&\
1563156&
$],
)

#sp Chiah ê siàu ha̍k-seng tio̍h sǹg.

#example[
+ $378125 times 16$; $543817 times 27$; $24678 times 35$; $765438 times 56$;
+ $593654 times 30$; $76824 times 302$; $847301 times 500$.
//p.19
+ $704745 times 6154$; $403006 times 18004$; $68905006 times 190018$.
+ Chiàu nn̄g-ê hoat-tō͘ lâi siong-sêng $6427301 times 63$; $42504 times 56$; $3020015 times 72$; $327901 times 36$.
+ $687010 times 100$; $6001500 times 20100$; $3010300 times 1002$.
+ $1644405 times 7749$; $1389294 times 8900$; $463098 times 7380$.
+ *Phia̍h-tsó* tsài-phia̍h, múi-tsāi $8350$ tè, chiū tsa̍p-gō͘ tsāi lóng-kiōng kúi tè? #h(1fr)Tek $125250$.
+ Bó͘-lâng múi-ji̍t kiâⁿ-lō͘ $75$ lí, chiū $17$ ji̍t ōe kiâⁿ kúi-lí? #h(1fr)Tek $1275$ lí.
+ Bóe-bah múi-kun tio̍h $96$ chîⁿ, chiū $23$ kun tio̍h lóng-kiōng jōa-tsōe? #h(1fr)Tek $2208$ chîⁿ.
+ Ke-lāi só͘-hùi chi̍t-ji̍t $429$ chîⁿ, chiū $38$ ji̍t tio̍h jōa-tsōe? #h(1fr)Tek $16302$ chîⁿ.
+ Chi̍t-ji̍t ū $24$ tiám-cheng, chiū $98$ ji̍t lóng-tsóng ū kúi-ê tiám-chheng? #h(1fr)Tek $2352$.
+ Chi̍t-ji̍t ū $24$ tiám-cheng, chi̍t-ê tiám-cheng $60$ hun chi̍t-hun $60$ biáu chiū chi̍t-lé-pài ($7$ ji̍t) kúi-biáu? #h(1fr)Tek $604800$.
+ Chi̍t-nî $365$ ji̍t, chiū tsa̍p-nî kúi-biáu? #h(1fr)Tek $315360000$.
+ Siat-sú chi̍t-kho͘-gûn ōe ōaⁿ-tit $1095$ chîⁿ, chiū $260$ kho͘ ōe ōaⁿ-tit jōa-tsōe? #h(1fr)Tek $284700$.
+ Chi̍t-ge̍h-ji̍t chhù-sè tio̍h $6360$ chîⁿ, chiū chi̍t-nî tio̍h jōa-tsōe? #h(1fr)Tek $76200$.
+ Siat-sú khui *hé-hun-chhia* lō͘, chi̍t-lí só͘-hùi $8396$ kho͘-gûn, chiū khui $375$ lí, só͘-hùi tio̍h jōa-tsōe? #h(1fr)Tek $3148500$.
+ Chi̍t-khek $15$ hun, chi̍t-hun $60$ biáu; Taⁿ mn̄g, Saⁿ-khek kúi biáu? #h(1fr)Tek $2700$.
+ Siat-sú $3000$ chîⁿ chi̍t-ge̍h-ji̍t tio̍h $66$ chîⁿ lī-sek, chiū tsa̍p-gō͘ ge̍h-ji̍t lī-sek tio̍h jōa-tsōe? #h(1fr)Tek $990$ chîⁿ
+ Siat-sú chi̍t-kho͘-gûn ge̍h-ge̍h jī-tsa̍p-jī chîⁿ lī-sek, chiū $34$ kho͘, poeh ge̍h-ji̍t, lī-sek tio̍h jōa-tsōe? #h(1fr)Tek $5984$ chîⁿ.
+ Lâng chiūⁿ-koe tia̍h-bí $8$ *chin*, chi̍t-chin $29$ chîⁿ; iū tah-iû $5$ kun, chi̍t-kun $128$ chîⁿ; iū bóe han-tsû-chhiam $13$ kun, chi̍t-kun $12$ chîⁿ; iū bóe bah $4$ kun, chi̍t-kun $96$ chîⁿ; taⁿ mn̄g, chí sì-tiâu lóng-tsóng khai jōa-tsōe? #h(1fr)Tek $1412$ chîⁿ.
+ Chi̍t-lâng chi̍t-ji̍t thàn pah-jī chîⁿ, chiū $25$ lâng $37$ ji̍t ōe thàn jōa-tsōe? #h(1fr)Tek $111000$ chîⁿ.
+ Bó͘ chheh kui-*phō* $34$ pún, múi-pún $76$ ia̍h, múi-ia̍h $18$ tsōa, múi-tsōa $21$ jī; Taⁿ mn̄g, kui-phō lóng-kiōng jōa-tsōe jī? #h(1fr)Tek $976752$.
+ Siat-sú tōe-kiû teh-se̍h ji̍t, chi̍t-tiám-cheng kú, kiâⁿ $204000$ lí, chiū chi̍t-ji̍t kiâⁿ kúi-lí? Koh chi̍t-nî kiâⁿ kúi-lí? #h(1fr)Tek $4896000$; $1787040000$.
//p.20
+ Siat-sú tùi tōe kàu ge̍h ū hé-hun-chhia ê lō͘, iā hé-hun-chhia, chi̍t-tiám-cheng kú kiâⁿ $160$ lí, tio̍h kiâⁿ nn̄g-pah-ji̍t chiah ōe kàu, chiū ge̍h lī tōe kúi-lí? #h(1fr)Tek $768000$.
+ Siat-sú tōa-chhèng ê chi̍t-lia̍p-chí, chi̍t-biáu-kú ōe kiâⁿ chi̍t-lí, chiū kiâⁿ liân-liân $9$ ji̍t kú ōe kàu-ge̍h, iā kiâⁿ $10$ nî chiah ōe kàu ji̍t-thâu: taⁿ sǹg chi̍t-nî $365$ ji̍t chiū ge̍h lī tōe kúi-lí? Koh ji̍t lī tōe kúi-lí? #h(1fr)Tek $777600$; $315360000$.
+ Siat-sú kng, chi̍t-biáu-kú ōe kiâⁿ $620130$ lí, iā kng tùi ji̍t kàu tōe tio̍h kiâⁿ $8$ hun koh $13$ biáu, (lóng-tsóng $493$ biáu) chiah ōe kàu; chiū ji̍t lī tōe kúi-lí? #h(1fr)Tek $305724090$.
+ Siat-sú tùi siōng-kūn ê chhiⁿ, kng tio̍h kiâⁿ sì-nî kú chiah ōe kàu-tōe, chiū siōng-kūn ê chhiⁿ lī tōe jōa-hn̄g? #h(1fr)$78225678720000$ lí.
+ Bó͘-lâng ū la̍k-kho͘-gûn, chít[chi̍t]-kho͘ ōaⁿ-tit $1146$ chîⁿ; chiong chîⁿ khì bóe $4$ tè *kau-í*, chít[chi̍t]-tè $560$ chîⁿ; nn̄g-tè-toh, chi̍t-tè $1455$ chîⁿ; *chián-pò͘* $17$ chhioh, chit[chi̍t]-chhioh $73$ chîⁿ: taⁿ mn̄g, chîⁿ iáu-chhun jōa-tsōe? #h(1fr)Tek $485$.
+ Bó͘-lâng *hē thn̂g* $6761$ tàⁿ, tsài khì Soaⁿ-tang *hoat-bōe*; chít[chi̍t] tàⁿ $3$ kho͘ pún. *Kàu-káng* chiū $860$ tàⁿ, bōe $4$ kho͘, *iáu-ê* bōe $5$ kho͘: taⁿ mn̄g, lóng-kiōng thàn kúi-kho͘? #h(1fr)Tek $12662$.
+ Bó͘-lâng beh tù-chheh, chít[chi̍t]-pún $252$ ia̍h, chit[chi̍t]-ia̍h $18$ tsōa, chit[chi̍t]-tsōa $19$ jī; chiū khì kap *ìn-pán-tiàm* siong-liông, *khek-pán* chit[chi̍t]-jī tio̍h nn̄g-ê-îⁿ, khek-liáu, *chheh-sin*, *pò͘-phê*, *ìn-kang*, *kap-kang*, múi-pún tio̍h $232$ chîⁿ: taⁿ mn̄g, $8000$ pún só͘-hùi lóng-kiōng tio̍h jōa-tsōe? #h(1fr)$2028368$ chîⁿ.
+ Bó͘-lâng khiàm-chè $583$ kho͘ bô gûn thang hêng; nā-sī ū saⁿ-chiah bé, múi-chiah ta̍t-tit $75$ kho͘, la̍k-chiah-gû, múi-chiah ta̍t-tit $18$ kho͘, jī-tsa̍p-it chiah ti, múi-chiah ta̍t-tit $3$ kho͘, saⁿ-tsa̍p-la̍k chiah iûⁿ, múi-chiah ta̍t-tit $2$ kho͘: chiū chiong chiah-ê　chêng-siⁿ lâi *kó͘-hêng*, iáu-khiàm kúi-kho͘? #h(1fr)Tek $115$,
]

