#import "../chheh.typ": chheh, example, example_equation,sp,cc
#let title = [PIT SOÀN Ê CHHO͘  HA̍K.]

#counter(page).update(4)
#pagebreak() //p.05?
#align(
    center,
    text(size:30pt, [
        #v(1.5em)
        #title
        #v(1em)
        ]),
)

Sǹg-siàu ê hoat-tō͘ m̄ nā chi̍t-ê. Kiám-chhái beh 
o̍h sǹg-siàu ê chho͘-ha̍k tiāⁿ-tiāⁿ chiū ēng sǹg-pôaⁿ
khiok ōe-tsòe tit, nā-sī beh káng-kiù sò͘-ha̍k khah-#cc
chhim ê lí, tio̍h kia̍h-pit lâi-siá chiah ōe.
#v(0.5em,weak:true)
Chiang-Tsoân chèng-lâng só͘-sǹg-ê, kan-ta sī chîⁿ-
gûn hit-hō siàu, chiū só͘-bat-ê, kan-ta sī sǹg-pôaⁿ ê
hoat-tō͘ tiāⁿ-tiāⁿ, só͘-í pêng-sî kiò chit-ê hoat-tō͘
tsòe *Soàn-hoat*. Kia̍h-pit lâi-siá iā chin-chiàⁿ sī soàn-#cc
hoat, chóng-sī, ài hun-piat-i, thang kiò-chòe *Pit-#cc
Soàn*.
#v(0.5em,weak:true)
Beh o̍h chit-ê Pit-Soàn ê hoat-tō͘ tio̍h tùi chho͘-
ha̍k khí, chiah thang chiām-chiām kàu *Tùi-sò͘ Tāi-#cc
sò͘*, *Sam-kak-hoat*, kap sò͘-ha̍k hiah ê khah-chhim
ê hong-hoat. Chiong chit-pún chho͘-ha̍k hō͘ ha̍k-#cc
seng o̍h, ì-sù m̄-sī beh hō͘-in, o̍h-liáu, thang kóng:
"Taⁿ bêng-pe̍k-lah"! sī beh hō͘-in lio̍h-lio̍h-á tsai sò͘-#cc
ha̍k ê ì-bī tiāⁿ-tiāⁿ, ǹg-bāng lâng-lâng ōe hìm-bō͘
koh chit-pún khah-chhim ê.
