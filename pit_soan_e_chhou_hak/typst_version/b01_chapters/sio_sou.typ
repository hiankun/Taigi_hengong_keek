#import "../chheh.typ": chheh, example, example_equation, tt, sp, cc

//p.28
= SIÓ-SÒ͘.

#sp Ēng sò͘-bo̍k kì-siàu, iàu-kín tio̍h chiong toaⁿ-ūi
*tsòe pún*. Tùi toaⁿ-ūi óa tsó-pêng lâi chìn chi̍t-ūi,
chiū ke siàu ê gia̍h tsa̍p-pē; koh chìn chi̍t-ūi, koh
ke tsa̍p-pē; í-siōng lóng án-ni. Chit ê hoat-tō͘ téng-#cc
bīn ū kóng-bêng, tsóng-sī ài ha̍k-seng *tì-ì* chiū koh
kóng-i.

#example_equation(
    5fr, 2fr,
[
#sp Siat-sú ū siàu $3456$, chit-tiâu-siàu chiū-sī $3$
chheng, koh $4$ pah, koh $5$ tsa̍p, koh $6$ ê. Nā-sī m̄-#cc
sái án-ni lēng-gōa kì: khòaⁿ i-ê ūi chiū tsai $5$ chiū-#cc
sī $5$ tsa̍p, $4$ chiū-sī $4$ pah, $3$ chiū-sī $3$ chheng.
],
[$
3000&\
 400&\
  50&\
   6&\
overline(3456)&
$],
)

#sp Taⁿ siat-sú toaⁿ-ūi ê iū-pêng iā ū só͘-kì ê siàu, tio̍h
iû-goân chiàu chit-ê hoat-tō͘ lâi hun-piat i-ê-ūi; chiū-#cc
sī tio̍h chiong toaⁿ-ūi tsòe pún, nā óa iū-pêng thè
chi̍t-ūi chiū chió siàu ê gia̍h tsa̍p-pē; koh thè chi̍t-#cc
ūi koh chió tsa̍p-pē; í-hā lóng án-ni.
#v(0.5em,weak:true)
Toaⁿ-ūi ê iū-pêng tio̍h ū *tō͘-tiám* thang hun-piat
ūi. Tō͘-tiám tsó-pêng ê siàu kiò-tsòe *Tōa-siàu*; tō͘-#cc
tiám iū-pêng ê siàu kiò-tsòe *Sió-sò͘*. Tō͘-tiám m̄ -sī
tsòe chi̍t-ūi, sī beh hō͘-lâng ōe jīn toaⁿ-ūi tiāⁿ-tiāⁿ.

#example[
Siat-sú ū siàu $3456 tt 789$. Tōa-siàu chiū-sī saⁿ-chheng sì-pah
gō͘-tsa̍p la̍k. Toaⁿ-ūi ê iū-pêng ū tō͘-tiám tsòe kì-hō. Tùi toaⁿ-#cc
ūi thè chi̍t-ūi ū $7$, chiū-sī tsa̍p-hūn ê chhit-hūn. Koh thè chi̍t-#cc
ūi ū $8$, chiū-sī chi̍t-pah-hūn ê $8$ hūn: koh thè chi̍t-ūi ū $9$, chiū-#cc
sī chi̍t-chheng-hūn ê $9$ hūn.
]

Tōa-siàu, tùi toaⁿ-ūi sǹg-khí lâi-chìn chiah ū tsa̍p-#cc
ê-ūi, pah-ê-ūi, chheng-ê-ūi, bān-ê-ūi, ûn-ûn. Sió-sò͘,
tùi toaⁿ-ūi sǹg-khí lâi-thè chiah ū tsa̍p-hūn ê ūi,
pah-hūn ê ūi, chheng-hūn ê ūi, bān-hūn ê ūi, ûn-ûn.

#pad(left:2em,
    [#example_equation(
        4fr, 2fr,
        [
        Beh siá $10$ hūn ê $1$ hūn, tio̍h siá\
        Beh siá $100$ hūn ê $1$ hūn, tio̍h siá\
        Beh siá $1000$ hūn ê $1$ hūn, tio̍h siá\
        Beh siá $1$, koh $10$ hūn ê $1$ hūn, tio̍h siá\
        Beh siá $10$, koh $1000$ hūn ê $6$ hūn, tio̍h siá\
        Beh siá $12$, koh $100$ hūn ê $24$ hūn, tio̍h siá\
        Beh siá $10000$ hūn ê $11$ hūn, tio̍h siá\
        Beh siá $1000$ hūn ê $101$ hūn, tio̍h siá
        ],
        [$
        &tt 1\
        &tt 01\
        &tt 001\
        1&tt 1\
        10&tt 006\
        12&tt 24\
        &tt 0011\
        &tt 101
        $],)
    ]
)

#example[
#sp Iàu-kín tio̍h kì-tit tō͘-tiám ê iū-pêng thâu-chi̍t-ūi chiū-sī tsa̍p-#cc
hūn ê ūi. Siat-sú ū siàu $.0123$ tio̍h m̄-thang lia̍h $0$ tsòe toaⁿ-ūi.
Chit-tiâu-siàu ê ūi tio̍h ho͘ kóng: tsa̍p-hūn, pah-hūn, chheng-#cc
hūn, bān-hūn, chiū-tsai chi̍t[chit?]-tiâu tsòe bān-hūn ê chi̍t-pah jī-tsa̍p
saⁿ hūn.
]

Chiah ê siàu ha̍k-seng tio̍h liān kàu-se̍k.

#example[
+ Chí kúi-tiâu-siàu tio̍h tha̍k, hō͘ sian-siⁿ thiaⁿ. $tt 5$, $tt 05$, $tt 005$, $tt 004$, $tt 0104$, $tt 1004$, $1 tt 2345$.
+ $1 tt 0101$, $11 tt 111$, $40 tt 00004$, $1 tt 1$, $ tt 2$, $4 tt 5$, $2 tt 024$.
+ $tt 6$, $tt 06$, $tt 008$, $tt 0079$, $tt 00023$, $tt 0102$.
+ Che kúi-nā tiâu-siàu tio̍h ēng sò͘-bo̍k siá: Tsa̍p-hūn ê saⁿ-hūn; Pah-hūn ê la̍k-tsa̍p-sì hūn; Chheng-hūn ê poeh-hūn; Bān-hūn ê la̍k-á-it-hūn; Tsa̍p-bān-hūn ê chhit-hūn.
+ Tsa̍p-hūn ê tsa̍p-it-hūn; Tsa̍p-hūn ê chi̍t-pah-liân-it hūn; Chheng-hūn ê chi̍t-pah-liân-it-hūn.
+ Tsa̍p-bān-hūn ê saⁿ-hūn; Chheng-bān hūn ê la̍k-hūn; Pah-bān-hūn ê tsa̍p-sì-hūn.
]

#sp Kiám-chhái siàu-lāi ū sió-sò͘, chiū lâi ōaⁿ tō͘-tiám
hō͘-i-thè chi̍t-ūi, nn̄g-ūi, saⁿ-ūi, che-sī ná chhin-#cc
chhiūⁿ ēng $10$, $100$, $1000$ lâi sêng hit-ê siàu. Hō͘
tō͘-tiám chìn chi̍t-ūi, nn̄g-ūi, saⁿ-ūi, sī ná chhin-#cc
chhiūⁿ ēng $10$, $100$, $1000$ lâi pun hit ê siàu.

#example[
Siat-sú ū siàu $1234 tt 5678$, chiū lâi ōaⁿ tō͘-tiám hō͘-i thè chi̍t-#cc
ūi, só͘ tit-tio̍h ê siàu $12345 tt 678$ lóng ke $10$ pē: $1$ pún tsòe $1$
chheng, taⁿ tsòe $1$ bān, $2$ pún tsòe $2$ pah, taⁿ tsòe $2$ chheng, $3$
pún tsòe $3$ tsa̍p, taⁿ tsòe $3$ pah, $4$ pún tsòe $4$ ê, taⁿ tsòe $4$ tsa̍p,$5$ pún tsòe tsa̍p-hūn ê $5$ hūn, taⁿ tsòe $5$ ê, $6$ pún tsòe pah-hūn
ê $6$ hūn, taⁿ tsòe tsa̍p-hūn ê $6$ hūn, í-hā lóng án-ni; khó-kiàn
hō͘ tō͘-tiám thè chi̍t-ūi sī ná chhin-chhiūⁿ ēng $10$ lâi sêng-siàu.
Koh hō͘ tō͘-tiám thè nn̄g-ūi sī ná chhin-chhiūⁿ ēng $100$ lâi
sêng-siàu.
]

//p.30
#example[
Kiám-chhái hō͘ tō͘-tiám chìn chi̍t-ūi, chiū pún-siàu pìⁿ-tsòe
$123 tt 45678$, chiū-sī chió $10$ pē; khó-kiàn ōaⁿ tō͘-tiám hō͘-i chìn
chi̍t-ūi sī ná chhin-chhiūⁿ ēng $10$ lâi pun hit-ê siàu.
]

#v(0.5em,weak:true)
Sió-sò͘ ê iū-pêng lâi thiⁿ khòng, i-ê gia̍h iā bô ke-#cc
thiⁿ, iā bô kiám-chió: nā-sī tsó-pêng lâi thiⁿ khòng,
chió siàu ê gia̍h $10$ pē, ná chhin-chhiūⁿ ēng $10$ lâi
pun-i chi̍t-iūⁿ.

#v(0.5em,weak:true)
#example[
Siat-sú sió-sò͘ $tt 3$, chiū-sī tsa̍p-hūn ê saⁿ-hūn. Iū-pêng lâi
thiⁿ $0$, chiū tsòe $tt 30$, chiū-sī pah-hūn ê $30$ hūn $=$ tsa̍p-hūn ê $3$
hūn, chiū-sī $= tt 3$. Tsó-pêng lâi thiⁿ $0$, chiū tsòe $tt 03$, chiū-sī pah-#cc
hūn ê saⁿ-hūn, pí tsa̍p-hūn ê saⁿ-hūn chió $10$ pē.
]

#v(0.5em,weak:true)
Thang hō͘ ha̍k-seng liān ê siàu.

#v(0.5em,weak:true)
#example[
1. $1 tt 1$, $tt 3$, $tt 00125$, $1 tt 01$. Che sì-tiâu-siàu, sûi-tiâu tio̍h ēng $100$, $1000$, $10000$, $1000000$ lâi sêng-i, chiū-sī tio̍h chiàu-hoat ōaⁿ tō͘-tiám lâi kì-siàu.
#v(0.5em,weak:true)
#h(1fr)Tek $110$, $1100$, $11000$, $1100000$, $30$, ûn-ûn.
2. Iā tio̍h ēng $100$, $1000$, $10000$, $1000000$ lâi pun-i, chiū-sī tio̍h ōaⁿ tō͘-tiám lâi kì-siàu.
#v(0.5em,weak:true)
#h(1fr)Tek $tt 011$, $tt 0011$, $tt 00011$, $tt 0000011$, $tt 003$, ûn-ûn.
]

