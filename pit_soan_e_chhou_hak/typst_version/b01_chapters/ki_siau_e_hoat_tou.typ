#import "../chheh.typ": chheh, example, example_equation, sp, cc

= KÌ-SIÀU Ê HOAT-TŌ͘.

#sp Beh hun-piat siàu ê tōa-sòe, iàu-kín tio̍h khòaⁿ
sò͘-bo̍k kúi-ūi, iā tio̍h tùi tiāⁿ-tio̍h ê ūi lâi sǹg-khí.
Sǹg-khí tio̍h tùi chiàⁿ-pêng, chiū thâu-chi̍t-ūi kiò-#cc
tsòe *toaⁿ* ê ūi, tē-jī, tsòe *tsa̍p* ê ūi, tē-saⁿ tsòe *pah* ê
ūi, tē-sì tsòe *chheng* ê ūi, tē-gō͘ tsòe *bān* ê ūi.

#example[
Siat-sú siá $35$. Chit-tiâu-siàu ū nn̄g-ūi. Toaⁿ ê ūi ū $5$, chiū-#cc
sī gō͘-ê. Tsa̍p ê ūi ū $3$, chiū-sī saⁿ-tsa̍p, só͘-í chit-tiâu-siàu tio̍h
tha̍k: saⁿ-tsa̍p-gō͘.
#v(0.5em,weak:true)
Siat-sú siá $409$. Chit-tiâu-siàu ū saⁿ-ūi. Toaⁿ ê ūi ū $9$, chiū-#cc
sī káu-ê. Tsa̍p ê ūi ū $0$, chiū-sī khòng. Pah ê ūi ū $4$, chiū-sī sì-#cc
pah. Só͘-í chit-tiâu-siàu tio̍h tha̍k sì-pah khòng káu-ê, á, sì-pah
liân káu-ê.
#v(0.5em,weak:true)
Siat-sú siá $12345$. Chit-tiâu-siàu ū gō͘-ūi. Toaⁿ ê ūi ū $5$, chiū-#cc
sī gō͘-ê. Tsa̍p ê ūi ū $4$, chiū-sī sì-tsa̍p. Pah ê ūi ū $3$, chiū-sī saⁿ-#cc
pah. Chheng ê ūi ū $2$, chiū-sī nn̄g-chheng. Bān ê ūi ū $1$, chiū-#cc
sī chi̍t-bān. Só͘-í chit-tiâu-siàu tio̍h tha̍k: chi̍t-bān nn̄g-chheng
saⁿ-pah sì-tsa̍p-gō͘.
]

Ēng sò͘-bo̍k lâi kì-siàu, lóng tio̍h chiàu chit-ê
hoat-tō͘. Toaⁿ ê ūi tsòe-*pún*: nā chìn chi̍t-ūi chiū ke
siàu ê gia̍h tsa̍p pē: koh chìn chi̍t-ūi, chiū koh ke
//p.07
hit-ê gia̍h tsa̍p-pē. Kiám-chhái siàu bô kàu toaⁿ-ūi,
chiū tio̍h kì $0$ lâi lâu i ê ūi: í-siōng lóng tio̍h án-ni,
chhin-chhiūⁿ ē-bīn ū pâi-lia̍t.

$
           4 & op(" sì-ê")\
          44 & op(" sì-tsa̍p-sì")\
         444 & op(" sì-pah sì-tsa̍p-sì")\
        4004 & op(" sì-chheng liân sì-ê")\
       40040 & op(" sì-bān liân sì-tsa̍p")\
      400000 & op(" sì-tsa̍p-bān")\
     4000000 & op(" sì-pah-bān")\
    40000000 & op(" sì-chheng-bān")\
   400000004 & op(" sì-ek liân sì-ê")\
  4000000000 & op(" sì-tsa̍p-ek")\
 40000000000 & op(" sì-pah-ek")\
400000000000 & op(" sì-chheng-ek")
$

#sp Iū-koh chìn, chiah ū *tiāu*, tsa̍p-tiāu, pah-tiāu,
chheng-tiāu, *keng*, tsa̍p-keng, ûn-ûn kàu *kai*, *ché*,
*jióng*, *ko͘*, *kàn*, *chèng*, *tsài*, *ke̍k*.

Ē-bīn chiah-ê-siàu, sûi-tiâu tio̍h tha̍k
#example[
#set enum(indent: 3em)
+ $25$; $50$; $150$; $106$; $1000$; $10001$; $10010$.
+ $560040$; $4110404$; $600060006$.
+ $108060$; $46287538162832586450$.
]

#sp Ē-bīn chiah-ê-siàu tio̍h, sûi-tiâu, ēng sò͘-bo̍k siá.

#example[
+ Jī-tsa̍p; saⁿ-tsa̍p-it; sì-tsa̍p-jī; gō͘-tsa̍p-saⁿ; la̍k-tsa̍p-sì; chhit-tsa̍p-gō͘; poeh-tsa̍p-la̍k; káu-tsa̍p-chhit.
+ Pah-gō͘; chi̍t-pah-liân-gō͘; nn̄g-pah-it; saⁿ-á-saⁿ; poeh-pah-tsa̍p-it; káu-á-la̍k; chhit-pah-jī-tsa̍p-káu.
+ Chi̍t-chheng; nn̄g-bān; saⁿ-tsa̍p-bān; gō͘-chheng-bān; la̍k-ek; chhit-pah-ek; poeh-chheng-ek; káu-tiāu; chi̍t-pah-keng; sì-tsa̍p-kai.
+ Saⁿ-bān liân la̍k-tsa̍p-sì; sì-chheng-poeh; nn̄g-pah-ek liân saⁿ-chheng-jī; chi̍t-chheng nn̄g-pah saⁿ-tsa̍p-sì-tiāu gō͘-chheng-la̍k-pah-ek káu-chheng chi̍t-pah jī-tsa̍p-saⁿ-bān. Sì-chheng tiāu liân gō͘-pah; sì-chheng-keng saⁿ-ek liân gō͘-ê.
+ Saⁿ-kai liân gō͘-pah; sì-keng poeh chheng chhit-pah sì-tsa̍p-jī-tiāu liân gō͘-pah-ek. Sì-ek liân la̍k-ê; la̍k-pah-ek saⁿ-bān liân la̍k-pah-la̍k.
]

//p.08
#sp Chí la̍k-ê kì-hō iā tio̍h ha̍k-si̍p kàu ōe jīn-bêng sòa siá hó-khòaⁿ.

#set terms(indent: 0.5em, hanging-indent: 3em)
/ : //FIX: typst bug?
/ $+$ (thiⁿ): ì-sù chiū-sī iū-chhiú ê siàu tio̍h thiⁿ-chham tsó-chhiú-ê, khòaⁿ kiōng jōa-tsōe.
/ $-$ (kiám): ì-sù chiū-sī iū-chhiú ê siàu tio̍h tio̍h tùi tsó-chhiú-ê khàu-khí-lâi, khòaⁿ chhun jōa-tsōe.
/ $times$ (siong-sêng): ì-sù chiū-sī iū-chhiú tsó-chhiú ê siàu tio̍h siong-sêng khòaⁿ tit-tio̍h jōa-tsōe.
/ $div$ (pun): ì-sù chiū-sī tio̍h ēng iū-chhiú ê siàu lâi pún tsó-chhiú-ê.
/ $=$ (pîⁿ-pîⁿ): ì-sù chiū-sī iū-chhiú, tsó-chhiú ê siàu pîⁿ-tōa.
/ $therefore$ (só͘-í):.

#example[
#pad(left:2em,
    [
    Siat-sú siá $3 + 4 = 7$, ì-sù chiū-sī $3$ kap $4$ kiōng tsòe $7$.\
    $5 - 3 = 2$ ì-sù chiū-sī $5$ kiám $3$ chhun $2$.\
    $5 times 4 = 20$ ì-sù chiū-sī $5$ kap $4$ siong-sêng chiū tit-tio̍h $20$.\
    $20 div 4 = 5$ ì-sù chiū-sī, ēng $4$ lâi pun $20$ chiū tit-tio̍h $5$; in-ūi $20$ chiok $4$ gō͘-pē.
    ],
)
]

