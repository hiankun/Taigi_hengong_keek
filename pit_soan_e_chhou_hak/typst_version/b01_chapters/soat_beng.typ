#import "../chheh.typ": bl

#pagebreak()

= Soat Bêng

- "Pit Soàn ê Chho͘ Ha̍k" ū hun "ts" kap "ch".

- Ēng *chho͘ kut* ê jī kù, sī góa katī siat ê phiaukì. Ìsù sī "ē tàng kā i o̍h khílâi" á sī "choanbûn ê jī kù".

- Nā sī goân chheh ū khólêng siá têngtâⁿ ê só͘chāi, tiō iōng \[?\] lâi chù.  Chhiūⁿ:
#move(dx:1em,dy:0em,
    block(
        width: 90%,
        fill: luma(228),
        inset: 8pt,
        radius: 4pt,
        [..., siong-sêng só͘ tit ê siàu bô chêng[cheng?]-tsoa̍h.],
))

- Nā sī góa tha̍k bô, koh chhōe bô chuliāu ê kù, tiō iōng #text(red, [âng sek ê jī]). Chhiūⁿ:
#move(dx:1em,dy:0em,
    block(
        width: 90%,
        fill: luma(228),
        inset: 8pt,
        radius: 4pt,
        [Tio̍h án-ni tùi-hā #text(red, [oe-sin]) kàu siōng.]
))

- Nńgthé kesi:
  - Pâi pán: #bl("https://typst.app/docs", "typst") (https://typst.app/docs).
  - Phah jī: #bl("https://gitlab.com/hiankun/poj_ibus_table", "poj_ibus_table") (https://gitlab.com/hiankun/poj_ibus_table).


#v(1fr)
#h(1fr) Tēⁿ Hiàn Kun, 2023-05-13
