#import "../chheh.typ": chheh, example, example_equation, sp, cc

= SÒ͘ BO̍K.

#sp Beh kia̍hk-pit lâi sǹg-siàu tio̍h ū tiāⁿ-tio̍h ê Sò͘-#cc
bo̍k thang-siá. Chit-pún-chheh-lāi só͘-ēng-ê pún sī
*A-lat-pek kok* chhut; āu-lâi pa̍t-kok ê lâng, khòaⁿ-#cc
i put-chí hó-ēng lâi o̍h-i.
//p.06
Chit-hō ê sò͘-bo̍k kiōng ū tsa̍p-ê, chiū-sī:

#move(dx:4em,
    [
    #stack(dir:ltr,spacing:2em,
        [
        0, khòng.\
        1, chi̍t.\
        2, nn̄g.\
        3, saⁿ. 
        ],
        [
        4, sì.\
        5, gō͘.\
        6, la̍k.
        ],
        [
        7, chhit.\
        8, poeh.\
        9, káu.
        ],
    )]
)

#sp Chiah-ê sò͘-bo̍k tio̍h ha̍k-si̍p kàu ōe jīn-bêng sòa-#cc
siá hó-khòaⁿ. Siá, lóng tio̍h pîⁿ-tōa.

#example[
Beh siá khah-tōa-tiâu ê siàu, chiū sò͘-bo̍k chi̍t-ê bô kàu. Tùi
tsa̍p í-chiūⁿ, sò͘-bo̍k tio̍h nn̄g-ê, tùi pah í-chiūⁿ tio̍h saⁿ-ê; chhin-#cc
chhiūⁿ $10$, it-tsa̍p, $100$, chi̍t-pah.
]
