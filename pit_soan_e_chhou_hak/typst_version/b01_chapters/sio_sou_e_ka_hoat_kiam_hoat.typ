#import "../chheh.typ": chheh, example, example_equation, tt, sp, cc

= SIÓ-SÒ͘ Ê KA-HOAT, KIÁM-HOAT.
#v(0.5em,weak:true) //FIX
#sp Kiám-chhái ū sió-sò͘ kúi nā tiâu siàu, ài siong-ka
khòaⁿ ha̍p-kiōng jōa-tsōe, chiū hi̍t-ê hoat-tō͘ kap
tōa-siàu ê hoat-tō͘ saⁿ-tâng. Pâi-lia̍t siàu ê sî iàu-#cc
kín tio̍h hō͘ tō͘-tiám lóng tsòe it-ti̍t lo̍h-lâi, chiū toaⁿ-#cc
ūi ōe tùi toaⁿ-ūi, tsa̍p-hūn ê ūi ōe tùi tsa̍p-hūn ê ūi,
pah-hūn ê ūi ōe tùi pah-hūn ê ūi, í-hā lóng án-ni.
Kiám-chhái ū siàu bô sió-sò͘ chiū tio̍h ēng khòng lâi
*that* i-ê ūi, khah ōe chiàu chhù-sù[sū?].

#v(0.5em,weak:true)
#example_equation(
    5fr,2fr,
[
#sp Siat-sú ū la̍k tiâu siàu, $2 tt 81464$, $tt 0938$, $8$, $tt 875$, $31 tt 2788$, $4 tt 0087$.
#v(0.5em,weak:true)
Tio̍h pâi-lia̍t chhin-chhiūⁿ piⁿ-thâu, hō͘ tō͘-#cc
tiám tsòe it-ti̍t lo̍h-lâi, chiong $0$ lâi that tē-saⁿ
tiâu kap tē sì-tiâu ê khang ūi.
#v(0.5em,weak:true)
Lâi siong-ka sī tú-á chhin-chhiūⁿ tōa-siàu chi̍t-#cc
iūⁿ, iā só͘ tit-tio̍h ê siàu hit-ê tō͘-tiám tio̍h kap
lóng-tsóng ê tō͘-tiám tsòe it-ti̍t lo̍h lâi.
],
[$
 2 &tt 8146\
   &tt 0938\
 8 &tt 0000\
   &tt 8750\
31 &tt 2788\
 4 &tt 0087\
overline(47 &tt 0709)
$],
)

//p.31
#sp Sió-sò͘ Kiám-hoat iā-sī chhin-chhiūⁿ tōa-siàu ê
Kiám-hoat chi̍t-iūⁿ. Iàu-kín tio̍h hō͘ tō͘-tiám tsòe
it-ti̍t lo̍h-lâi, chiū sûi-ūi chiàu chhù-sù[sū?]. Kiám-liáu
chhun-siàu hit-ê tō͘-tiám iā tio̍h kap pún-#cc
siàu ê tō͘-tiám tsòe it-ti̍t lo̍h-lâi.

#example_equation(
    5fr, 2fr,
[
#sp Siat-sú beh tùi $8 tt 04$ khàu $4 tt 006$ khí-lâi Tio̍h pâi-#cc
lia̍t chhin-chhiūⁿ piⁿ-thâu ê khoán-sit, ēng $0$ lâi
that téngs-lia̍t ê khang ūi, khàu-tû si̍t tit-tio̍h $4 tt 034$.
#v(0.5em,weak:true)
Koh, siat-sú tùi $1$ beh khàu $tt 063$ khí-lâi. Pâi-#cc
lia̍t hó, lâi-kiám, chiū tit-tio̍h $tt 937$.
],
move(dx:3em,dy:-2em,
    [$
8 &tt 040\
4 &tt 006\
overline(4 &tt 034)\
\
1 &tt 000\
  &tt 063\
overline(&tt 937)\
$]),
)

#v(-1.0em,weak:true)
#sp Chiah ê siàu ha̍k-seng tio̍h pâi-lia̍t lâi sǹg.

#example[
1. $11 tt 275 + tt 34132 + tt 00414 + tt 0001 + 23 tt 001$.
#v(0.3em,weak:true)
#h(1fr)Tek $34 tt 62157$.
2. $321 tt 4 + 12 + 31 tt 6154 + tt 01 + 2 tt 214 + 415 tt 62$.
#v(0.3em,weak:true)
#h(1fr)Tek $782 tt 8594$.
3. $tt 001213 + 45 tt 613 + 234 + tt 0012 + 141 tt 00056$.
#v(0.3em,weak:true)
#h(1fr)Tek $420 tt 615973$.
4. $1 tt 0000123 + 31 tt 1 + 117 tt 154 + 2343 tt 008 + tt 0002$.
#v(0.3em,weak:true)
#h(1fr)Tek $2492 tt 2622123$.
5. $32 tt 001 - 12 tt 999$. #h(1fr)Tek $19 tt 002$.
6. $3 tt 45 - tt 00098$. #h(1fr)Tek $3 tt 44902$.
7. $3 tt 412 - 2 tt 99987$. #h(1fr)Tek $tt 41213$.
8. $22 tt 0001 - 2 tt 99994$. #h(1fr)Tek $19 tt 0002$.
9. $tt 001 - tt 00099874$. #h(1fr)Tek $tt 0000013$.
10. $24 tt 004 - tt 9875164$; $tt 0123 - tt 009807$.
#v(0.3em,weak:true)
#h(1fr)Tek $23 tt 016484$; $tt 003213$.
]
