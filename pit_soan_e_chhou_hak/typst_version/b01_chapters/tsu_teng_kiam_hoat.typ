#import "../chheh.typ": chheh, example, example_equation, tt, sp, cc

//p.55
= TSU-TÉNG KIÁM-HOAT.

#sp Tsu-téng kiám-hoat chiū-sī tsu-téng ka-hoat ê
tùi-hoán tiāⁿ-tiāⁿ.

Kiám-chhái gia̍h-siaⁿ lóng chiàu-tsa̍p chìn, chiū
tio̍h siá bú tsòe tōa-siàu, kiáⁿ tsòe sió-sò͘; chiàu sió-#cc
sò͘ ê hoat-tō͘ lâi kiám-i. Kiám-chhái bô chiàu-tsa̍p
chìn chiū *chèng-siàu*, *chhù-téng* tio̍h hoâiⁿ-lia̍t siá,
tùi-lō͘-bé kiám-khí. Bó͘-ūi ê goân-siàu bô kàu-gia̍h
thang kiám, chiū tio̍h chioh bú ê hoat-siàu thiⁿ pún-#cc
ūi ê goân-siàu, lâi-kiám, kì só͘-chhun-ê. Hêng bú-#cc
siàu ē-lia̍t só͘-chioh-ê, chiū-sī bú-siàu ê chi̍t, koh
kiám, kì só͘-chhun-ê. Ta̍k-ūi tio̍h chhin-chhiūⁿ àn[án?]-#cc
ni. Kàu-thâu chiū-soah.

#example_equation(5fr,1fr,
[
#sp Siat-sú bó͘-lâng khiàm-gûn $12$ niú $6$ hun-pòaⁿ,
chiū lâi hêng $8$ niú $4$ chîⁿ; iáu khiàm jōa-tsōe?
Gia̍h-siaⁿ lóng chiàu-tsa̍p chìn, chiū tio̍h kì niú
tsòe tōa-siàu, chîⁿ, hun, tsòe sió-sò͘. Chiàu-hoat
lâi kiám, tit-tio̍h $3$ niú $6$ chîⁿ $6$ hun-pòaⁿ.
],
[$
12 tt 065&\
underline(#h(0.5em)8 tt 400)&\
3 tt 665&
$],
)

#example_equation(3fr,2fr,
[
#sp Siat-sú bó͘-lâng ū mī-hún $15$ kun $6$ niú
$9$ chîⁿ, lâi bōe $10$ kun $12$ niú $5$ chîⁿ: iáu-#cc
chhun jōa-tsōe?
],
move(dx:2em,dy:-1em,
[$
op("Kun") #h(2.0em) op("Niú") #h(2.0em) op("Chîⁿ") &\
#h(0.5em) 15 #h(3.5em) 6 #h(3.0em) 9 #h(0.5em)&\
underline(#h(0.5em) 10 #h(3.0em) 12 #h(3.0em) 5 #h(0.5em))&\
#h(0.5em) 4 #h(3.0em) 10 #h(3.0em) 4 #h(0.5em)&\
$],
)
)
#v(-0.5em,weak:true)
#example[
#sp Pâi-lia̍t hó, tùi chîⁿ ê ūi kiám-khí. Chîⁿ-#cc
ūi ê $9$ kiám $5$, chhun $4$. Pún-ūi kì $4$. Niú-#cc
ūi ê $5$ kiám $12$, bô kàu-gia̍h thang kiám,
chiū tio̍h chioh kun-ūi ê $1$ hòa tsòe $16$ niú, thiⁿ-chham goân-#cc
siàu ê $6$ niú, kiōng $22$; kiám $12$, chhun $10$. Pún-ūi-ē tio̍h kì $10$.
Kun-ūi ê $15$ kiám $10$ kap só͘-chioh ê $1$, kiōng kiám $11$, chhun
$4$. Kun-ūi-ē tio̍h kì $4$.
]

#example[
#sp Tùi $3$ nî $210$ ji̍t $18$ tiám-cheng $40$ hun $50$ biáu kiám $1$ nî $220$
ji̍t $20$ tiám-cheng $30$ hun $56$ biáu.
]
#v(0.5em,weak:true)
#example_equation(1fr, 1fr,
[
#sp Pâi-lâi hó, tùi biáu ê ūi
kiám-khí. $50$ kiám $56$, bô
thang-kiám, chiū tio̍h chioh $1$
hun hòa-tsòe $60$ biáu, thiⁿ-#cc
chham pún-ūi ê $50$, kiōng $110$,
kiám $56$, chhun $54$. Pún-ūi-ē tio̍h kì $54$.
],
[$
op("Nî") #h(1.5em) op("Ji̍t") #h(1em) op("Tiám") #h(1em) op("Hun") #h(1em) op("Biáu")&\
#h(0.5em) 3 #h(1.6em) 210 #h(2.0em) 18 #h(1.6em) 40 #h(2.0em) 50 #h(0.5em)&\
underline(
    #h(0.5em) 1 #h(1.6em) 220 #h(2.0em) 20 #h(1.6em) 30 #h(2.0em) 56 #h(0.5em)
)&\
#h(0.5em) 1 #h(1.6em) 354 #h(2.0em) 22 #h(2.1em)  9 #h(2.0em) 54 #h(0.5em)&\
$]
)
#example[
                         Hun-ūi ê $40$ kiám $30$
chham só͘-chioh ê $1$, kiōng kiám $31$, chhun $9$. Tiám-ūi ê $18$
//p.56
kiám $20$ bô thang kiám, tio̍h chioh $1$ ji̍t, hòa-tsòe $24$ tiám-#cc
cheng, thiⁿ-chham goân-siàu ê $18$, kiōng $42$, kiám $20$ chhun $22$.
Ji̍t-ūi ê $210$ kiám $220$ chham só͘-chioh ê $1$, kiōng-kiám $221$, bô
thang-kiám, tio̍h chioh $1$ nî, hòa-tsòe $365$ ji̍t, thiⁿ-chham pún-#cc
ūi ê goân-siàu $210$, kiōng $575$, kiám $221$, chhun $354$. Nî-ūi ê
$3$ kiám $1$ chham só͘-chioh ê $1$, kiōng-kiám $2$, chhun $1$.
]

#sp Chiah ê siàu ha̍k-seng tio̍h sǹg.

#example[
1. Chi̍t-niú kiám gō͘-chîⁿ, chhun jōa-tsōe? Kiám gō͘-hun, chhun jōa-tsōe? Kiám gō͘-lî, chhun jōa-tsōe? #h(1fr)Tek $tt 5$, $tt 95$, $tt 995$.
2. Chi̍t-niú kiám $5$ chîⁿ $5$ hun-pòaⁿ, kiám $9$ chîⁿ, kiám $1$ lî, sûi-tiâu chhun jōa-tsōe? #h(1fr)Tek $tt 445$, $tt 1$, $tt 999$.
3. $5$ chîⁿ kiám $5$ hun, kiám $1$ hun-pòaⁿ, kiám pòaⁿ-hun, sûi-tiâu-siàu chhun jōa-tsōe? #h(1fr)Tek $tt 45$, $tt 485$, $tt 495$.
4. $10$ kho͘-gûn kiám $9$ kho͘ $9$ kak $9$ chiam $9$, iáu-chhun jōa-tsōe?
#v(0.5em,weak:true)
#h(1fr)Tek $tt 001$.
5. $11$ kho͘-gûn kiám $10$ kho͘ $9$ kak pòaⁿ, kiám $1$ kho͘ liân $5$ chiam, kiám $10$ kho͘ $5$ chiam, sûi-tiâu iáu-chhun jōa-tsōe? #h(1fr)Tek $tt 05$, $9 tt 95$, $tt 95$.
6. $15$ kho͘ liân pòaⁿ-chiam kiám $10$ kho͘ $5$ kak $4$, kiám $4$ kho͘ liân $5$ chiam, kiám $9$ kho͘ $4$ kak pòaⁿ, sûi-tiâu iáu-chhun jōa-tsōe?
#v(0.5em,weak:true)
#h(1fr)Tek $4 tt 465$, $10 tt 955$, $5 tt 555$.
7. $12$ chio̍h $2$ kap kiám $8$ chio̍h $6$ táu, kiám $8$ chio̍h $6$ chin, kiám $8$ chio̍h $6$ kap, sûi-tiâu-siàu iáu-chhun jōa-tsōe?
#v(0.5em,weak:true)
#h(1fr)Tek $3 tt 402$, $3 tt 942$, $3 tt 996$.
8. Bó͘-lâng ū sòaⁿ $100$ tn̄gj $5$ chhioh, chiū ēng $35$ tn̄g $8$ chhioh $3$ chhùn sàng gín-ná, thang pàng hong-chhe, *āu-chhiú* iáu-chhun jōa-tsōe?
#v(0.5em,weak:true)
#h(1fr)Tek $64 tt 67$.
9. Bó͘-lâng tòa $15$ kho͘-gûn, lo̍h koe-chhī bóe pò͘ $3$ kho͘ $7$ kak-pòaⁿ, bóe pe̍h-thn̂g $6$ kak $7$, bóe ôe $2$ kho͘ $2$ kak $3$, bóe han-tsû $5$ kak $6$, tia̍h-bí $4$ kho͘ $4$ kak $4$, tah-iû $3$ kak-pòaⁿ, bóe lân-san-ê $1$ kho͘ $2$ kak: iáu-chhun jōa-tsōe? #h(1fr)Tek $1 tt 8$.
10. Tân khiàm Lîm $1010$ kho͘-gûn chiū *tùi-toaⁿ* $990$ kho͘ $5$ kak-pòaⁿ, tio̍h koh *òe*-gûn jōa-tsōe chiah thang *chheng-siàu*?
#v(0.5em,weak:true)
#h(1fr)Tek $19 tt 45$.
11. $2$ lé-pài $4$ ji̍t $16$ tiám-cheng $44$ hun $18$ biáu kiám $1$ lé-pài $6$ ji̍t $20$ tiám-cheng $56$ hun $22$ biáu, chhun kúi-ji̍t?
#v(0.5em,weak:true)
#h(1fr)Tek $4$ $19$ $47$ $56$.
12. Hé-hun-tsûn tùi Ē-mn̂g kiâⁿ kàu Si̍t-la̍t, tsúi-lō͘ tio̍h $6$ ji̍t $23$ tiám-cheng $56$ hun $45$ biáu, iā koh tio̍h hioh Hiong-káng $1$ ji̍t $16$ tiám-cheng $24$ hun; taⁿ í-keng lī-khui Ē-mn̂g $8$ ji̍t $14$ tiám-cheng $50$ hun $15$ biáu, tio̍h koh kúi-tiám-cheng chiah ōe kàu Si̍t-la̍t? #h(1fr)Tek $1$ $30$ $30$.
]
//p.57
